#!/usr/bin/env python
#
# Authors:  John G. Hagedorn
#               (http://www.nist.gov/cgi-bin/wwwph?John+Hagedorn)
#               Scientific Applications and Visualization Group, NIST
#               (The one who did all the coding)
#
#           Joshua A. Taillon
#               (http://www.joshuataillon.com)
#               University of Maryland, College Park
#
#           Lourdes G. Salamanca-Riba
#               (http://www.mse.umd.edu/faculty/salamanca-riba)
#               University of Maryland, College Park
#
# ####
#
# Instructions:
#
# To calculate TPB lengths,    ./tpbLen [options] imagefile0 imagefile1 . . . .
#
#    options:
#
#      -s x y z    - size of a voxel
#      -S voxSzFN  - specifies the name of a file that
#                      contains the voxel size
#
# The image files can be either image files (they don't have to be TIFFs), or
#  you can specify the name of a file that contains a list of image file
#  names. This is the way that I've been running it. When I run it,
#  the command line looks like this:
#
#      ./tpbLen -S voxSize.dat imageList.txt
#
#
# ####
#
# This program finds triple-phase boundaries (TPB) in a 3D volume of
# data that has been segmented into three phases. The boundary edges
# are found, connected, and TPB lengths are calculated and reported.
# 
# 
# TPB lengths are calculated by the centroid method described in:
# 
#    Naoki Shikazono, Daisuke Kanno, Katsuhisa Matsuzaki, Hisanori Teshima,
#    Shinji Sumino, and Nobuhide Kasagi, 
#    "Numerical Assessment of SOFC Anode Polarization Based
#    on Three-Dimensional Model Microstructure Reconstructed
#    from FIB-SEM Images",
#    Journal of The Electrochemical Society, 157 (5) B665-B672, 2010.
#
# The centroid method described in this paper has been extended to handle
# path branching.
# 
# The TPB paths are classified by methods described in these papers:
#    
#    James R. Wilson, Anh T. Duong, Marcio Gameiro, Hsun-Yi Chen, Katsuyo 
#    Thornton, Daniel R. Mumm, Scott A. Barnett, "Quantitative three-
#    dimensional microstructure of a solid oxide fuel cell cathode",
#    Electrochemistry Communications 11 (2009) 1052-105.
#    
#    James R. Wilson, Marcio Gameiro, Konstantin Mischaikow, William Kalies, 
#    Peter W. Voorhees, Scott A. Barnett, "Three-Dimensional Analysis of 
#    Solid Oxide Fuel Cell Ni-YSZ Anode Interconnectivity", Microscopy and
#    Microanalysis 15, 71-77, 2009.
#
####
#
# A voxel edge is considered a TPB edge if the four neighboring voxels 
# include all three phases. So we find these edges and then we connect 
# the edges into paths and calculate path length by a centroid method 
# based on the work of Shikazono, et al.
# 
# As mentioned above, we also classify each path. To do this, we first 
# classify voxels then classify each path edge by the class of the voxels 
# to which it is adjacent, then classify each path based on the classes 
# of its edges.
# 
# Voxel classification is done by the scheme of the two Miller papers.
# The idea is to find the connected components (CC) of voxels of each phase, 
# then classify each CC. 
#
# By the way, I sometimes use the word "category" as synonym for class or 
# classification in the comments below. I do this in order not to get
# confused with the Python "class" concept.
# 
# There are three classes that a CC can fall into:
# 
#     Across   : CC intersects 2 or more of the six volume boundaries
#     Dead End : CC intersects 1 volume boundary
#     Isolated : CC intersects no volume boundary
# 
# These classifications can be applied to the connected component and to
# the voxels contained in the connected component. Each voxel is in one
# and only one CC.
#
# The three phases are all treated the same. In the Miller papers, Across
# is sometimes called "connected" or "active".
# 

ACROSS = 1
DEAD_END = 2
ISOLATED = 4

# Each TPB edge is classified based on the classes of the four voxels
# that it contacts:
# 
#     Inactive: the edge contacts any voxel of an Isolated CC
#     Unknown : the edge does not contact any Isolated voxel, but does
#                 contact a Dead End voxel
#     Active  : the edge contacts only Across voxels
# 

CLASS_NONE = 0
ACTIVE = 1
UNKNOWN = 2
INACTIVE = 4

# Each TPB path is a set of connected TPB edges. Each TPB edge is in one
# and only one TPB path. We use the same classifications for TPB paths that
# we use for TPB edges. The class of a TPB path is determined by the classes 
# of its edges like this:
# 
#     Active  : All edges are Active
#     Unknown : All edges are either Active or Unknown
#     Inactive: Some edges are Inactive
# 
#
#
# Voxels are identified by i,j,k indices.
#
# For convenience and for the bulk of this code I'm going to think of 
# voxel (i,j,k) as being an axis-aligned cube that is one unit on a side 
# centered at xyz coordinates (i+0.5, j+0.5, k+0.5). So one corner of the
# cube will be at xyz coordinates (i,j,k); I'll refer to this as the 
# lower corner of the voxel. The corner diagonally across the cube from
# this will be at xyz coordinates (i+1, j+1, k+1).
#
# A voxel edge can be identified as an i,j,k index and one of 
# three directions x, y, or z. The edge ((i, j, k), dir) refers
# to the point at the lower corner of voxel (i,j,k) and the direction
# indicates which of the three coordinates is incremented to get to
# the other end point of the edge.  So edge ((2,4,7), y) has end
# points with xyz coordinates (2,4,7) and (2,5,7).
# 
# The above correspondence between voxels (and voxel edges) and xyz 
# coordinates is used in the code that finds and connects TPB edges. 
# When the code calculates TPB path length, these coordinates are 
# modified to account for differing voxel sizes in the three dimensions.
# 
# 
# The code is generally arranged with the functions in C-like
# order with the higher-level functions near the bottom. 
#
# I suspect that some of my coding is not very pythonic in spirit; that's
# the breaks.

import getopt
import sys
import math
import optparse
import argparse
# import progressbar
from tqdm import tqdm

from datetime import datetime


# numpy reference: http://docs.scipy.org/doc/numpy/reference/
import numpy as np  # I like to use np.foo instead of numpy.foo

from scipy import ndimage

# PIL Image documentation: http://effbot.org/imagingbook/image.htm
from PIL import Image


# --------------------------------------------------------------------        
# --------------------------------------------------------------------        
# This code below creates a graph of nodes and connections.  The nodes 
# correspond to lattice locations in the original volume. The connections
# are TPB edges that connect two nodes. Each node has a set of connections
# to other nodes and each connection points back at the two end point nodes.
# Each connection also has data members that store centroids that are
# calculated based on the locations of the end points and the centers
# of adjacent connections. (Two connections are adjacent if they share
# a common end point.)
# 
# The comments in the code sometimes refer to the "root" of the graph.
# This is simply one of the nodes on the graph. The graphs are not
# trees; they could contain loops.
# 
# --------------------------------------------------------------------        

class PtConnection:
    def __init__(self, endPtA, endPtB):
        self.ptNode = list((None, None))
        self.ptNode[0] = endPtA
        self.ptNode[1] = endPtB
        self.edgeClass = CLASS_NONE
        self.centroid = np.zeros([2, 3])
        self.centroidsAreEqual = False

        self.center = np.array([
            (endPtA.i + endPtB.i) / 2.0,
            (endPtA.j + endPtB.j) / 2.0,
            (endPtA.k + endPtB.k) / 2.0
        ])
        self.visited = False

    def __str__(self):
        infostr = ""
        infostr += "PtConnection:\n"
        infostr += "   " + self.ptNode[0]
        infostr += "   " + self.ptNode[1]
        infostr += "   center: " + self.center
        infostr += "   centroid[0] = %f %f %f\n" % \
                   self.centroid[0, 0], self.centroid[0, 1], self.centroid[
                       0, 2]
        infostr += "   centroid[1] = %f %f %f\n" % \
                   self.centroid[1, 0], self.centroid[1, 1], self.centroid[
                       1, 2]


# end of class PtConnection
# --------------------------------------------------------------------        

class PtNode:
    def __init__(self, ii, jj, kk):
        self.i = ii
        self.j = jj
        self.k = kk
        self.connections = set()
        self.loc = np.array([ii, jj, kk])

    def __init__(self, indx):
        self.i = indx[0]
        self.j = indx[1]
        self.k = indx[2]
        self.connections = set()
        self.loc = np.array(list(indx))

    def __str__(self):
        infoStr = ""
        infoStr += "PtNode: "

        infoStr += " i, j, k = %d %d %d      num connections %d " % \
                   (self.i, self.j, self.k, len(self.connections))

        return infoStr


# end of class PtNode
# --------------------------------------------------------------------        

class PtGraph:
    def __init__(self):
        self.startPtNode = None
        self.allPtNodes = dict()
        self.pathCategory = None


# end of class PtGraph

# --------------------------------------------------------------------        

def tupleAdd(a, b):
    return map(lambda a, b: a + b, a, b)


def tupleMult(a, b):
    return map(lambda a, b: a * b, a, b)


# --------------------------------------------------------------------

def outOfVol(vol, ind):
    for i in range(3):
        if ind[i] < 0:
            return True
        elif ind[i] >= vol.shape[i]:
            return True

    return False

# end of def outOfVol (vol, ind) :


# --------------------------------------------------------------------        

# These are used to indicate the direction of a voxel edge
EDGE_POS_X = 1
EDGE_POS_Y = 2
EDGE_POS_Z = 3
EDGE_NEG_X = 4
EDGE_NEG_Y = 5
EDGE_NEG_Z = 6


def tpbEdge(vol, classVol, node, edgeDir):
    # returns (edgeIsTpb, edgeClass)
    #   

    i = node.i
    j = node.j
    k = node.k

    # convert the negative edge directions to positive directions
    if edgeDir == EDGE_NEG_X:
        if i <= 0:
            return False, None
        i -= 1
        edgeDir = EDGE_POS_X

    elif edgeDir == EDGE_NEG_Y:
        if j <= 0:
            return False, None
        j -= 1
        edgeDir = EDGE_POS_Y

    elif edgeDir == EDGE_NEG_Z:
        if k <= 0:
            return False, None
        k -= 1
        edgeDir = EDGE_POS_Z

    # calculate the indices of the four voxels that will determine
    # whether the edge is a TPB and of what class

    ind11 = (i, j, k)

    if edgeDir == EDGE_POS_X:
        ind12 = (i, j, k - 1)
        ind21 = (i, j - 1, k)
        ind22 = (i, j - 1, k - 1)

    elif edgeDir == EDGE_POS_Y:
        ind12 = (i, j, k - 1)
        ind21 = (i - 1, j, k)
        ind22 = (i - 1, j, k - 1)

    elif edgeDir == EDGE_POS_Z:
        ind12 = (i, j - 1, k)
        ind21 = (i - 1, j, k)
        ind22 = (i - 1, j - 1, k)

    # check that the indices don't go outside the volume
    if outOfVol(vol, ind11):
        return False, None

    if outOfVol(vol, ind22):
        return False, None

    # The four voxels values
    a11 = vol[ind11]
    a12 = vol[ind12]
    a21 = vol[ind21]
    a22 = vol[ind22]

    # print "   surrounding voxels: ", a11, a12, a21, a22

    # OR together the voxels to determine if all are present, then
    #   see if either diagonal pair has different values
    isTpbEdge = (((a11 | a12 | a21 | a22) == 7) and
                 (a11 != a22) and (a21 != a12))

    if isTpbEdge:
        # figure out what class the edge is in
        c11 = classVol[ind11]
        c12 = classVol[ind12]
        c21 = classVol[ind21]
        c22 = classVol[ind22]
        catBits = (c11 | c12 | c21 | c22)
        if (catBits & ISOLATED) != 0:
            edgeClass = INACTIVE  # if any adjacent voxels are isolated
        elif (catBits & DEAD_END) != 0:
            edgeClass = UNKNOWN  # if any adjacent voxels are dead end
        else:
            edgeClass = ACTIVE  # all adjacent voxels are across
    else:
        edgeClass = None

    return isTpbEdge, edgeClass


# end of def tpbEdge (vol, classVol, node, edgeDir):
# --------------------------------------------------------------------        


def setPtGraphCategory(ptGraph):
    # This examines the category of each connection (edge) in the ptGraph 
    # and derives a category for the entire graph based on the category
    # of the edges.
    #
    # Recall that the edge categories are active, unknown, and inactive.
    # Paths are categorized the same way based on this scheme:
    #     Active  : All edges are Active
    #     Unknown : All edges are either Active or Unknown
    #     Inactive: Some edges are Inactive

    someUnknown = False
    # someInactive = False

    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections in epn and calc the centroids
        connections = epn.connections
        for conn in connections:
            if not conn.visited:
                if conn.edgeClass == INACTIVE:
                    ptGraph.pathCategory = INACTIVE
                    return 0

                elif conn.edgeClass == UNKNOWN:
                    someUnknown = True

    if someUnknown:
        ptGraph.pathCategory = UNKNOWN
    else:
        ptGraph.pathCategory = ACTIVE

    return 0


# end of def setPtGraphCategory (ptGraph):

# --------------------------------------------------------------------        

def findConnection(ptNodeA, ptNodeB):
    # Find if there is a PtConnection between these two PtNodes
    # Returns the connection or None

    # We only have to traverse the connections of one of the nodes
    # because a connection between two PtNodes is in the 
    # connection list of both of the nodes.

    for conn in ptNodeA.connections:
        # We know that one of the two ptNodes is ptNodeA, so we only
        # have to check if ptNodeB is present.
        if conn.ptNode[0] == ptNodeB:
            return conn
        elif conn.ptNode[1] == ptNodeB:
            return conn

    return None


# end of def findConnection (ptNodeA, ptNodeB)


# --------------------------------------------------------------------        

def buildPtGraph(ii, jj, kk, vol, classVol, visitedVol):
    # This builds a graph of PtNodes and PtConnections rooted at (ii, jj, kk)
    # If the edgeBit array indicates that there are any edges connected
    # to the vertex (ii,jj,kk), then this returns a PtNode that at (ii,jj,kk).
    # Otherwise it returns None.

    # print "Begin buildPtGraph at %d %d %d\n" % (ii, jj, kk)

    if visitedVol[ii, jj, kk]:
        return None

    ptGraph = PtGraph()

    ptGraph.startPtNode = PtNode([ii, jj, kk])

    # ptGraph.allPtNodes gives us a quick way of finding out if a PtNode is 
    # currently in the graph
    ptGraph.allPtNodes[(ii, jj, kk)] = ptGraph.startPtNode

    # currentNodes is a list of the nodes for which we need to add
    # connections to adjacent nodes; we initialize it to contain only 
    # the root node
    currentNodes = list()
    currentNodes.append(ptGraph.startPtNode)

    # These are constant tuples that we use to collapse the code into 
    # a loop over the six directions

    edgeFlags = (EDGE_POS_X, EDGE_POS_Y, EDGE_POS_Z,
                 EDGE_NEG_X, EDGE_NEG_Y, EDGE_NEG_Z)
    # negEdgeFlags = (EDGE_NEG_X, EDGE_NEG_Y, EDGE_NEG_Z,
    #                 EDGE_POS_X, EDGE_POS_Y, EDGE_POS_Z)
    offsets = ((1, 0, 0), (0, 1, 0), (0, 0, 1),
               (-1, 0, 0), (0, -1, 0), (0, 0, -1))

    # loop until we have followed all connected nodes
    while len(currentNodes) > 0:

        # get a node from the current node list
        node = currentNodes.pop()

        currInd = (node.i, node.j, node.k)

        # First check if the index is within the volume. 
        # Node indices outside the valid range of volume indices can 
        # occur at the positive faces of the volume.
        if not outOfVol(visitedVol, currInd):

            # Then check if if the node has been visited
            if not visitedVol[currInd]:

                # loop over each of the six directions that edges could occur
                for i in range(0, len(edgeFlags)):

                    (isTpb, edgeClass) = tpbEdge(vol, classVol, node,
                                                 edgeFlags[i])

                    # is there an edge in that direction?
                    if isTpb:

                        # get the index tuple for the connected node
                        connInd = tuple(tupleAdd(currInd, offsets[i]))

                        # see if the connected node is already present
                        connPt = ptGraph.allPtNodes.get(connInd)

                        if connPt is None:
                            # if connected node is not present, create it
                            connPt = PtNode(connInd)
                            # add new node to the list of nodes to be traversed
                            #   and to the set of all pt nodes
                            currentNodes.append(connPt)  # this forces another
                            # iteration of main loop
                            ptGraph.allPtNodes[connInd] = connPt
                            connection = None  # no connection currently exists

                        else:
                            # if connected node is present, see if there is a
                            # connection to the current node
                            connection = findConnection(node, connPt)

                        # At this point, we know that we have a tpb edge
                        # that goes between node and connPt.
                        # connection is either None or it is a PtConnection
                        # object that connects the two nodes.

                        # test if there is a connection object between the pts
                        if connection is None:
                            # if there is not a connection, create one
                            connection = PtConnection(node, connPt)
                            connection.edgeClass = edgeClass
                            # add it to the list of connections at each node
                            node.connections.add(connection)
                            connPt.connections.add(connection)

                            # end of if there an edge in that direction?

                # end of loop over each of the six directions that edges  could
                # occur

                visitedVol[currInd] = True

    # end of loop until we have followed all connected nodes

    setPtGraphCategory(ptGraph)

    # print "buildPtGraph at %d %d %d\n" % (ii, jj, kk)
    # print "   Number of nodes: ", len (ptGraph.allPtNodes)
    # print "   Number of connections to start node: ",
    # len(ptGraph.startPtNode.connections)

    # if we didn't find any connections at the start node, then return None
    if ((len(ptGraph.allPtNodes) == 1) and
            (len(ptGraph.startPtNode.connections) == 0)):
        return None

    return ptGraph


# end of def buildPtGraph (ii, jj, kk, vol, classVol, visitedVol)
# --------------------------------------------------------------------        

def connSetCentroid(connections):
    # Calculate the centroid of the center points of a set of 
    # connection segments.
    # Return the centroid as an np array of length 3.

    centroid = np.zeros(3)

    for conn in connections:
        centroid += conn.center

    centroid /= len(connections)

    return centroid


# end of def connSetCentroid (connections)

# --------------------------------------------------------------------        

def calcConnCentroids(voxelSize, conn):
    # calculate the centroid(s) at the connection

    endPtA = conn.ptNode[0]
    endPtB = conn.ptNode[1]
    connA = endPtA.connections
    connB = endPtB.connections

    if len(endPtA.connections) == 1:
        # starts at A
        if len(endPtB.connections) == 1:
            # path starts at A, ends at B
            conn.centroid[0] = endPtA.loc
            conn.centroid[1] = endPtB.loc
            conn.centroidsAreEqual = False

        elif len(endPtB.connections) == 2:
            # path starts at A, no branching at B
            conn.centroid[0] = endPtA.loc
            conn.centroid[1] = endPtA.loc
            conn.centroidsAreEqual = True

        else:
            # path starts at A, branching at B
            conn.centroid[0] = endPtA.loc
            conn.centroid[1] = connSetCentroid(endPtB.connections)
            conn.centroidsAreEqual = False

    elif len(endPtA.connections) == 2:
        # no branching at A
        if len(endPtB.connections) == 1:
            # no branching at A, ends at B
            conn.centroid[0] = endPtB.loc
            conn.centroid[1] = endPtB.loc
            conn.centroidsAreEqual = True

        elif len(endPtB.connections) == 2:
            # no branching at A, no branching at B
            conn.centroid[0] = connSetCentroid(connA.union(connB))
            conn.centroid[1] = conn.centroid[0]
            conn.centroidsAreEqual = True

        else:
            # no branching at A, branching at B
            conn.centroid[0] = connSetCentroid(endPtB.connections)
            conn.centroid[1] = conn.centroid[0]
            conn.centroidsAreEqual = True

    else:
        # branching at A
        if len(endPtB.connections) == 1:
            # branching at A, ends at B
            conn.centroid[0] = connSetCentroid(connA)
            conn.centroid[1] = endPtB.loc
            conn.centroidsAreEqual = False

        elif len(endPtB.connections) == 2:
            # branching at A, no branching at B
            conn.centroid[0] = connSetCentroid(connA)
            conn.centroid[1] = conn.centroid[0]
            conn.centroidsAreEqual = True

        else:
            # branching at A, branching at B
            conn.centroid[0] = connSetCentroid(connA)
            conn.centroid[1] = connSetCentroid(connB)
            conn.centroidsAreEqual = False

    conn.centroid[0] = tupleMult(conn.centroid[0], voxelSize)
    conn.centroid[1] = tupleMult(conn.centroid[1], voxelSize)

    return 0


# end of def calcConnCentroids (voxelSize, conn):
# --------------------------------------------------------------------        
def setConnectionVisitFlags(ptGraph, flag):
    # Traverse the pt/edge graph and set all connection visited flags

    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections in epn and calc the centroids
        connections = epn.connections
        for conn in connections:
            conn.visited = flag


# end of def setConnectionVisitFlags (ptGraph, flag):
# --------------------------------------------------------------------        

def calcCentroids(voxelSize, ptGraph):
    # Traverse the pt/edge graph and calculate the
    # edge centroid locations.

    # Actually, we're going to traverse the dict of ptGraph which lists
    # all of the point nodes; this gives us access to all of the edges,
    # which are called connections in the data structure.

    # first let's go to all of the connections and set the visited flags
    # to false.
    setConnectionVisitFlags(ptGraph, False)

    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections in epn and calc the centroids
        connections = epn.connections
        for conn in connections:
            if not conn.visited:
                calcConnCentroids(voxelSize, conn)
                conn.visited = True

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)


# end of def calcCentroids (voxelSize, ptGraph):
# --------------------------------------------------------------------        


def centroidSegLength(connections):
    # this is only called when connections has two elements
    conns = list(connections)
    a = conns[0]
    b = conns[1]

    if a.ptNode[0] == b.ptNode[0]:
        ia = 0
        ib = 0

    elif a.ptNode[0] == b.ptNode[1]:
        ia = 0
        ib = 1

    elif a.ptNode[1] == b.ptNode[0]:
        ia = 1
        ib = 0

    elif a.ptNode[1] == b.ptNode[1]:
        ia = 1
        ib = 1

    else:
        print >> sys.stderr, \
            "Fatal error while traversing TPB connections (B).\n"
        sys.exit(-1)

    return math.sqrt(
        (a.centroid[ia, 0] - b.centroid[ib, 0]) * (
            a.centroid[ia, 0] - b.centroid[ib, 0]) +
        (a.centroid[ia, 1] - b.centroid[ib, 1]) * (
            a.centroid[ia, 1] - b.centroid[ib, 1]) +
        (a.centroid[ia, 2] - b.centroid[ib, 2]) * (
            a.centroid[ia, 2] - b.centroid[ib, 2])
    )


# end of def centroidSegLength (connections)


# ----------------------------------------------------------------------
def sumSegmentLengths(ptGraph):
    # The TPB segments expressed in the ptGraph are of two types:
    #   a) segments that connect centroids of two connections that have
    #       common end points
    #   b) segments that connect the two centroids within a single connection
    #
    # Note that segments of type (a) only occur at ptNodes at which there 
    # there are exactly two connections. If there is only one connection, then
    # we are at the beginning or end of a TPB path.  If there are more than
    # two connections, then the centroid of the centers of all of the 
    # connected edges have been averaged together and placed into one of
    # the end points of each of the connections that converge at that 
    # point; thus there are no segments that need to be calculated around
    # that vertex.

    totalLength = 0.0

    # first find all segments of type (a) 
    for epn in ptGraph.allPtNodes.values():
        connections = epn.connections
        if len(connections) == 2:
            totalLength += centroidSegLength(connections)

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    # now look at each connection and sum the distances between the two 
    # centroids
    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections and sum up the distances 
        # between centroids
        connections = epn.connections
        for conn in connections:
            if not conn.visited:
                totalLength += centroidDistance(conn)
                conn.visited = True

    return totalLength


# end of def sumSegmentLengths (ptGraph) :
# --------------------------------------------------------------------        

def centroidDistance(conn):
    if conn.centroidsAreEqual:
        return 0.0

    return math.sqrt(
        ((conn.centroid[0][0] - conn.centroid[1][0]) *
         (conn.centroid[0][0] - conn.centroid[1][0])) +
        ((conn.centroid[0][1] - conn.centroid[1][1]) *
         (conn.centroid[0][1] - conn.centroid[1][1])) +
        ((conn.centroid[0][2] - conn.centroid[1][2]) *
         (conn.centroid[0][2] - conn.centroid[1][2])))


# end of def centroidDistance (conn)
# --------------------------------------------------------------------        


def measurePathLen(voxelSize, ptGraph):
    # return the length of the pt/edge graph rooted at epn
    calcCentroids(voxelSize, ptGraph)
    return sumSegmentLengths(ptGraph)


# end of def measurePathLen (voxelSize, ptGraph):
# --------------------------------------------------------------------        


def pathBranchLoop(ptGraph):
    # return the number of branches and loops in this path

    branches = 0
    loops = 1

    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections in epn and calc the centroids
        # connections = epn.connections
        if len(epn.connections) > 2:
            branches += 1
            loops = -1

        if len(epn.connections) < 2:
            loops = -1

    return branches, loops


# end of def pathBranchLoop (ptGraph):

# --------------------------------------------------------------------        


def measureAllTpbPaths(voxelSize, vol, classVol):
    visited = np.empty(vol.shape, dtype=np.bool)
    visited.fill(False)

    lengths = dict()
    lengths[ACTIVE] = list()
    lengths[INACTIVE] = list()
    lengths[UNKNOWN] = list()

    branches = dict()
    branches[ACTIVE] = list()
    branches[INACTIVE] = list()
    branches[UNKNOWN] = list()

    sys.stdout.flush()

    print "Processing connected components volume to find TPB paths:"
    # progressbar for monitoring completion
    # widgets = [progressbar.Bar('>'), ' ', progressbar.ETA(), ' ',
    #            progressbar.ReverseBar('<')]
    # bar = progressbar.ProgressBar(maxval=vol.shape[0],
    #                               widgets=widgets
    #                               ).start()

    # we loop through all voxels
    for i in tqdm(range(0, vol.shape[0]), desc='Processing slices'):
        # Progress monitor (using python-progressbar instead)
        # 2016-05-13: Changed to using tqdm
        # if (i % 10) == 0:
        #     # print "  At volume X slice ", i, " out of ", vol.shape[0]-1
        #     print "  At volume X slice %4d out of %d" % (i, vol.shape[0] - 1)
        #     sys.stdout.flush()

        for j in range(0, vol.shape[1]):
            for k in range(0, vol.shape[2]):
                if not visited[i, j, k]:
                    ptGraph = buildPtGraph(i, j, k, vol, classVol, visited)
                    if ptGraph is not None:
                        lengths[ptGraph.pathCategory].append(
                            measurePathLen(voxelSize, ptGraph))
                        (b, l) = pathBranchLoop(ptGraph)
                        branches[ptGraph.pathCategory].append(b)
                        writePtGraph(ptGraph, voxelSize)
        # bar.update(i)   # update progress bar

    # bar.finish()        # set progress bar to finish
    print ""
    sys.stdout.flush()

    # print "at end of measureAllTpbPaths  lengths = ", lengths
    return lengths, branches


# end of def measureAllTpbPaths (voxelSize, vol, classVol) :
# --------------------------------------------------------------------        

# ----------------------------------------------------------------------
def print_vol_subset(vol, corner, size):
    # ----------------------------------------------------------------------
    print "Vol shape is ", vol.shape
    print "   subset at corner ", corner, " with size ", size
    print vol[corner[0]:corner[0] + size, corner[1]:corner[1] + size, corner[
        2]:corner[2] + size]


# ----------------------------------------------------------------------
def remap_vol_vals(vol):
    # ----------------------------------------------------------------------
    """
    Check that the volume has only 3 different values (phases) and
    substitute 1 2 and 4 for these phases so that we can use bit-wise
    operations.

    vol should be a 3d array.
    """

    print "Remapping voxel values ..."
    uniqueVals = np.unique(vol)
    # print "Debug: unique values in volume = ", uniqueVals

    if len(uniqueVals) not in [3, 4]:
        # should we raise an exception? Or just quit?
        print >> sys.stderr, \
            "Error: Input must have exactly three phases (with an optional " \
            "\"0th\" phase; ", len(uniqueVals), " were found."
        sys.exit(-1)

    if 0 in uniqueVals:
        print "Voxels labeled 0 were found. These voxels will be ignored."
        uniqueVals = uniqueVals[uniqueVals != 0]
        # print "Debug: unique values in volume are now = ", uniqueVals

    # newVals remaps the original array values to values
    # that can be used in bit-wise operations.
    newVals = np.arange(255)
    newVals[uniqueVals[0]] = 1
    newVals[uniqueVals[1]] = 2
    newVals[uniqueVals[2]] = 4

    vol = newVals[vol]

    print "Done with remapping."
    print ""

    # print "Number of voxels in each phase: %d %d %d" % \
    # (vol[vol == 1].size, vol[vol == 2].size, vol[vol == 4].size)
    # print ""

    shp = vol.shape
    totVox = shp[0] * shp[1] * shp[2]
    print "Total number of voxels: ", totVox
    print "Number of voxels in each phase:"
    print "  orig val    remapped val    # voxels    % of total"
    num_vox = 0
    perc_vox = 0
    for i in range(0, 3):
        n = vol[vol == newVals[uniqueVals[i]]].size
        p = 100.0 * float(n) / float(totVox)
        num_vox += n
        perc_vox += p
        print (" %6d       %6d      %12d     %6.2f%%" %
               (uniqueVals[i],
                newVals[uniqueVals[i]],
                n,
                p))

    print "    total                  %12d    %6.2f%%" % (num_vox, perc_vox)

    # print_vol_subset (vol, [0,0,0], 10)

    return vol


# end of remap_vol_vals


# ----------------------------------------------------------------------
# Reads the open PIL image and puts the data into an
# np array that it appends to the end of the list images2D.
def loadImgData(img, images2D):
    # we assume that there may be multiple frames in the image
    # file, so we loop over all frames in the image.
    i = 0
    while True:
        try:
            # read the next frames
            img.seek(i)
            # print "Got frame ", i
            # print "   pixel 200,200 = ", img.getpixel( (200, 200))
            # print "   ", img

            # we assume that all frames have the same size
            # if i == 0:
            #     size2D = img.size

            # create the np array and put the image data in it.
            # i2D = np.empty(size2D, dtype=np.int8)
            i2D = np.asarray(img)

            # put the np array onto the list of image data arrays
            images2D.append(i2D)
            # print "   i2D[20, 20] = ", i2D[20, 20] 

            i += 1

        except EOFError:
            # we've run out of frames
            # print "Failed at frame ", i
            break

    # print "count of images2D = ", len (images2D)

    return True


# end of testImgRead

# ----------------------------------------------------------------------

# Read the file to get a list of image file names then
#  read image image file and put each 2D image into 
#  images2D.
# Return True for success, False for failure.
def loadImgFileListData(fn, images2D):
    # read the file for a list of image file names

    fnList = list()

    try:
        # read all lines into a list
        f = open(fn)
        lines = f.readlines()
        f.close()

    except EnvironmentError as err:
        print >> sys.stderr, \
            "Error occurred while reading input file ", fn
        print >> sys.stderr, "     ", str(err)
        return False

    # go through each line and split into list of tokens; this list
    # should be a list of image file names.
    for line in lines:
        fnList = fnList + line.split()

    # print (" loadImageFileListData fnList = ", fnList)

    # load the image files in the list
    return loadImages(fnList, images2D)  # recursion


# ----------------------------------------------------------------------

# Read each file in the the list of file names.
# Each name in the ist is either an image file or a file
# that contains a list of image files.
# As it reads image files, it accumulates the data in
# a list of 2D np arrays.
# Returns True for success, False for failure.
def loadImages(inFnList, images2D):
    for fn in inFnList:
        # print "loadImages fn = ", fn

        try:
            # try to open as an image and read data.
            img = Image.open(fn)
            # print img
            if not loadImgData(img, images2D):
                return False

        except EnvironmentError:
            # if PIL can't open it, then try to read it as
            # a list of file names.
            if not loadImgFileListData(fn, images2D):
                return False

    return True


# end of loadImages
# ----------------------------------------------------------------------


def printVolSize(vol, voxelSize):
    shp = vol.shape
    print "Voxel spacing is %.3f x %.3f x %.3f" % voxelSize
    print "Volume is %d x %d x %d voxels" % shp
    print "Volume physical dimensions are %.2f x %.2f x %.2f" % \
          (shp[0] * voxelSize[0], shp[1] * voxelSize[1], shp[2] * voxelSize[2])
    print ""

# end of def printVolSize (vol, voxelSize):

# ----------------------------------------------------------------------
# Create a volume (a 3D np array) from a list of file names.
# Each file should either be an image file or a file that
# contains a list of image files.
#
# Returns the np array if successful; returns None otherwise.
#


'''
    This code introduces a slight oddness in the indexing of the volume.
    The default storage order for np arrays is C ordering; this means
    that the last index varies fastest as you move linearly through
    the array.  But the image data comes in with the pixels arranged
    with the horizontal dimension varying fastest.

    If the images are all W x H pixels and there are N images, then
    the resulting volume is a H x W x N np array. Note the reversal
    of W and H.  

    We could fix this easily enough, and perhaps I will do so in 
    a subsequent revision.  Probably we should make the volume a 
    W x H x N np array. (Revise by using the np transpose function;
    i.e. np.transpose(np.asarray(img), (1,0)). )

    Note also that the input images should be 8-bit grayscale images.
    If they aren't in the form, they can be converted to that format
    with the ImageMagick convert command that looks something like:

        convert -colorspace gray -depth 8   4.png   4.tif
'''


def loadVolFromFileList(inFnList, voxelSize):
    print "Reading input image files ...."

    # we create a list of np 2D arrays with the image data
    # for each image
    images2D = list()

    if not loadImages(inFnList, images2D):
        return None  # error

    # print "count of images2D = ", len (images2D)

    # create the numpy 3D array with to receive the list of 2D images

    sz = images2D[0].shape

    # vol = np.empty((sz[0], sz[1], len(images2D)), dtype=np.int8)
    vol = np.empty((sz[1], sz[0], len(images2D)), dtype=np.int8)

    try:
        # loop over the 2D images and put them in the 3D array
        for i in xrange(len(images2D)):
            # assign the 2D image data into a z-slice of the 3D array
            # vol[:,:,i] = images2D[i]
            vol[:, :, i] = np.transpose(images2D[i], (1, 0))

    except IndexError as err:
        print >> sys.stderr, "Error : Bad input image dimensions?"
        print >> sys.stderr, "        ", str(err)
        return None  # failure - probably because the dimensions don't match

    print "Done reading input image files."
    print ""

    '''
    shp = vol.shape
    print "Voxel spacing is %.3f x %.3f x %.3f" % voxelSize
    print "Volume is %d x %d x %d voxels" % shp
    print "Volume physical dimensions are %.2f x %.2f x %.2f" % \
      (shp[0]*voxelSize[0], shp[1]*voxelSize[1], shp[2]*voxelSize[2])
    print ""
    '''
    printVolSize(vol, voxelSize)

    # print_vol_subset (vol, [0,0,0], 5)

    vol = remap_vol_vals(vol)

    # print_vol_subset (vol, [0,0,0], 5)

    return vol  # success
# end of loadVolFromFileList
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def findAdjacents(currVal, vol, used, indx):
    # ----------------------------------------------------------------------
    #   vol - volume of voxels values
    #   used - boolean volume indicating whether the voxel
    #          has been used as an adjacent
    #   indx - indices of the voxel for which we want adjacents
    #
    # Note that we are only looking at face-adjacent voxels.
    # There are at most six possibilities; fewer if indx is on
    # the boundary of the volume.

    adj = list()

    print "In findAdjacents: indx = ", indx[0], indx[1], indx[2]

    for i in range(0, 3):

        if indx[i] > 0:
            a = list(indx)
            a[i] -= 1
            if not used[a[0], a[1], a[2]]:
                if vol[a[0], a[1], a[2]] == currVal:
                    print "   In findAdjacents: setting True at  %d %d %d", \
                        a[0], a[1], a[2]
                    used[a[0], a[1], a[2]] = True
                    adj.append(a)

        if indx[i] < (vol.shape[i] - 1):
            a = list(indx)
            a[i] += 1
            # print "before test a = ", a
            # print "before test used[a...] = ", used[a[0], a[1], a[2]]
            if not used[a[0], a[1], a[2]]:
                if vol[a[0], a[1], a[2]] == currVal:
                    print "   In findAdjacents: setting True at  %d %d %d", \
                        a[0], a[1], a[2]
                    used[a[0], a[1], a[2]] = True
                    # print "after test used[a...] = ", used[a[0], a[1], a[2]]
                    adj.append(a)
                    # print "appended a = ", a
                    # print "adj = ", adj

    return adj


# end of def findAdjacents (vol, used, indx)

# ----------------------------------------------------------------------
def labelAllConnectedVoxels(iStart, jStart, kStart, vol, cc, used):
    # ----------------------------------------------------------------------

    # OK, we want to do this without recursing.
    # I'm going to create a list of adjacent voxels, then traverse
    # the list. During traversal, I may add other adjacent voxels
    # as connected voxels are found. Each adjacent voxel is removed
    # from the list as it's connectivity is determined.

    # Note: we're doing the check against currVoxVal twice, once in
    #       findAdjacents and once here. This should be fixed if we
    #       want to continue to use these functions.

    print "\n\n\n******* Begin labelAllConnected Voxels at %d %d %d" % (
        iStart, jStart, kStart)

    currVoxVal = vol[iStart, jStart, kStart]
    currVoxLabel = cc[iStart, jStart, kStart]

    print "  currVoxVal = %d    currVoxLabel = %d" % (currVoxVal, currVoxLabel)

    numberOfVoxels = 1

    used[iStart, jStart, kStart] = True

    adjacents = list()
    adjacents += findAdjacents(currVoxVal, vol, used, [iStart, jStart,
                                                       kStart])
    # print "adjacents at top of labelAllConnectedVoxels : ", adjacents

    while len(adjacents) > 0:

        # print "len(adjacents) = %d" % len(adjacents)

        # print "adjacents at top of find loop : ", adjacents
        indx = adjacents.pop(0)
        # print "curr indx = (%d %d %d): " % tuple(indx)

        # print "currVoxVal = %d" % currVoxVal
        # print "vox val = %d at %d %d %d " % (vol[indx[0], indx[1],
        # indx[2]],  indx[0], indx[1], indx[2])

        if vol[indx[0], indx[1], indx[2]] == currVoxVal:
            cc[indx[0], indx[1], indx[2]] = currVoxLabel

            numberOfVoxels += 1

            if (numberOfVoxels % 10000) == 1:
                print "number of voxels = ", numberOfVoxels

            # We only add voxels to the list of adjacent voxels
            # when they are adjacent to a known member of the current
            # connected component.
            adjacents += findAdjacents(currVoxVal, vol, used, indx)
            # print "adjacents after find (%d %d %d): " % tuple(indx),
            # adjacents
            # print "\n\n"
    # that's it?

    print "End labelAllConnected Voxels at %d %d %d\n\n\n" % (iStart,
                                                              jStart, kStart)
    print "numberOfVoxels = %d", numberOfVoxels

    return numberOfVoxels


# end of def labelAllConnectedVoxels (i, j, k, vol, cc)

# ----------------------------------------------------------------------

def classifyVolume(vol):
    # print "********************* BEGIN CLASSIFY VOLUME *********************"
    print ""
    print "Beginning classification of volume connected components ..."

    # Returns a list with two np arrays. Each array has the same shape as the 
    # incoming array of voxels.  The two arrays are:
    #
    # category: an np array with same shape as vol. Each entry is an 
    #           integer giving the category of the voxel and its CC
    #
    # ccInfo: is a dict such that for category i (starts at 1) :
    #           ccInfo[i] is a list of two integers {cat, num}:
    #                 cat is the category of the CC
    #                 num is the number of voxels in the CC
    #

    # This array gives an integer tag identifying the CC or each component
    # CC tags will start with 1, so zero indicates that the voxel has 
    # not been tagged.
    cc = np.zeros(vol.shape, dtype=np.uint32)

    # The array of categories: ACROSS, DEAD_END, or ISOLATED
    category = np.empty(vol.shape, dtype=np.uint8)
    category.fill(0)

    ccInfo = dict()

    used = np.empty(vol.shape, dtype=np.bool)
    used.fill(False)

    # We're going to fill in cc first, then we use that
    # to figure out the categories

    # In cc 0 means that its status is undetermined, anything
    # else is a tag identifying the particular connected component of which 
    # the voxel is a member.

    # I suspect that there is some extremely clever pythonic way for
    # constructing the connected components, but for now, I'm just 
    # going to loop over the voxels

    # nextComponentTag = 1

    # print_vol_subset (vol, [0,0,0], 5)
    # print_vol_subset (cc, [0,0,0], 5)
    # print_vol_subset (used, [0,0,0], 5)

    '''
    I'm replacing this section with a method based on 
    scipy.ndimage.measurements.label
    '''

    '''
    # we loop through all voxels
    for i in range (0, vol.shape[0]):
        print "In classifyVolume at i = %d" % i
        print "\n full volume"
        print_vol_subset (vol, [0,0,0], 5)
        print "\n cc volume"
        print_vol_subset (cc, [0,0,0], 5)
        print "\n used volume"
        print_vol_subset (used, [0,0,0], 5)
        for j in range (0, vol.shape[1]):
            for k in range (0, vol.shape[2]):
                # we only look at voxels that have not yet been tagged as
                # part of an existing connected component
                print "i j k = ", i, j, k
                if cc[i,j,k] == 0 :
                    # we start a new connected component
                    cc[i,j,k] = nextComponentTag

                    numVox = labelAllConnectedVoxels (i, j, k, vol, cc, used)

                    ccInfo[nextComponentTag] = (ISOLATED, numVox)
                    nextComponentTag += 1

                    # print "JUST BEFORE END"
                    # print_vol_subset (cc, [0,0,0], 5)
                    # print_vol_subset (used, [0,0,0], 5)
                    # sys.exit(-1) 

    totalComponents = nextComponentTag



    '''

    # print "--------------------------"
    # print "before label"

    nf = 0

    altVol = np.zeros(vol.shape, dtype=np.uint8)

    altVol[vol == 1] = 1
    labeled, numFeat = ndimage.measurements.label(altVol)
    print "shape of labelled ", labeled.shape
    cc += labeled.astype('uint32')
    # print "after label 1"
    # print "Number of voxels in phase 1 = ", labeled[labeled != 0].size
    # print "Number of CC in phase 1 = ", numFeat

    nf += numFeat
    altVol.fill(0)
    altVol[vol == 2] = 2
    labeled, numFeat = ndimage.measurements.label(altVol)
    # print "Number of voxels in phase 2 = ", labeled[labeled != 0].size
    # print "Number of CC in phase 2 = ", numFeat
    labeled[labeled != 0] += nf
    cc += labeled.astype('uint32')
    # print "after label 2"

    nf += numFeat
    altVol.fill(0)
    altVol[vol == 4] = 4
    labeled, numFeat = ndimage.measurements.label(altVol)
    # print "Number of voxels in phase 4 = ", labeled[labeled != 0].size
    # print "Number of CC in phase 4 = ", numFeat
    labeled[labeled != 0] += nf
    cc += labeled.astype('uint32')

    nf += numFeat

    totalComponents = nf

    # print "********************* MID CLASSIFY VOLUME ***********************"
    # print "total number of components = ", totalComponents
    # print "Here's the final cc:"
    # print_vol_subset (cc, [0,0,0], 5)

    # Now fill in ccInfo with the default category and number of voxels
    # in each CC.

    counts = np.bincount(np.reshape(cc, -1))
    for i in range(totalComponents + 1):
        # print "Counting CC ", i, counts[i]
        ccInfo[i] = (ISOLATED, counts[i])

    # JGH: could check sum of counts against the size of vol and
    #      that counts[0] is zero and that 
    #      length of counts is totalComponents+1

    # print "Done with counting of number of voxels in each CC."

    # OK, at this point the array cc labels each voxel with a unique
    # integer tag that identifies the connected component of which
    # it is part.

    # from each of the six sides of CC we can make a set of the unique
    # component tags that are present on that side
    tagSet = dict()
    tagSet[0, 0] = set(np.reshape(cc[0, :, :], -1))
    tagSet[0, 1] = set(np.reshape(cc[vol.shape[0] - 1, :, :], -1))

    tagSet[1, 0] = set(np.reshape(cc[:, 0, :], -1))
    tagSet[1, 1] = set(np.reshape(cc[:, vol.shape[1] - 1, :], -1))

    tagSet[2, 0] = set(np.reshape(cc[:, :, 0], -1))
    tagSet[2, 1] = set(np.reshape(cc[:, :, vol.shape[2] - 1], -1))

    # Then can look at the intersections of the six tag sets to determine
    # the class of each CC

    # Recall the definitions of the voxel CC categories:
    #     Across   : CC intersects 2 or more of the six volume boundaries
    #     Dead End : CC intersects 1 volume boundary
    #     Isolated : CC intersects no volume boundary

    # We create python sets that contain the CC tags for across and dead end
    # categories

    acrossSet = set()

    # We look at the intersection of every pair of sides and add that into
    # acrossSet.
    # This does every pair intersection twice; no problem but does it 
    # affect speed?
    for i in range(3):
        for ii in range(3):
            for j in range(2):
                for jj in range(2):
                    if (ii != i) | (jj != j):
                        # add to acrossSet every tag that is on two faces
                        acrossSet |= tagSet[i, j] & tagSet[ii, jj]

    # deadEndSet is the set of all tags on all boundaries with the
    # set of tags in acrossSet removed.
    deadEndSet = (tagSet[0, 0] | tagSet[0, 1] |
                  tagSet[1, 0] | tagSet[1, 1] |
                  tagSet[2, 0] | tagSet[2, 1]
                  ) - acrossSet

    # Note that in the ccInfo dict above, we have labeled all CCs as ISOLATED.
    # We're now going to change those entries for the tags in the acrossSet
    # and the deadEndSet

    ccLUT = np.empty([totalComponents + 1], dtype=np.uint32)
    ccLUT.fill(ISOLATED)

    for tag in acrossSet:
        info = ccInfo[tag]
        ccInfo[tag] = (ACROSS, info[1])
        ccLUT[tag] = ACROSS

    for tag in deadEndSet:
        info = ccInfo[tag]
        ccInfo[tag] = (DEAD_END, info[1])
        ccLUT[tag] = DEAD_END

    nAcross = len(acrossSet)
    nDeadEnd = len(deadEndSet)
    nIsolated = totalComponents - (nAcross + nDeadEnd)
    pcAcross = 100.0 * float(nAcross) / float(totalComponents)
    pcDeadEnd = 100.0 * float(nDeadEnd) / float(totalComponents)
    pcIsolated = 100.0 * float(nIsolated) / float(totalComponents)

    print "Done with classification of volume connected components."
    print ""
    print "Summary of connected components:"
    print "    Number of Across   : %6d  %6.2f%%" % (nAcross, pcAcross)
    print "    Number of Dead End : %6d  %6.2f%%" % (nDeadEnd, pcDeadEnd)
    print "    Number of Isolated : %6d  %6.2f%%" % (nIsolated, pcIsolated)
    print "    Total              : %6d" % totalComponents

    print ""
    print ""

    '''
    # now we use this info to fill in the category array
    for i in range (0, vol.shape[0]):
        for j in range (0, vol.shape[1]):
            for k in range (0, vol.shape[2]):
                category[i,j,k] = ccInfo[cc[i,j,k]][0]
    '''

    '''
    category = np.take (ccLUT, cc)
    '''

    category = ccLUT[cc]

    # print "********************* END CLASSIFY VOLUME ***********************"

    return category, ccInfo


# end of def classifyVolume (vol) :

# ----------------------------------------------------------------------

def calc_tpb(voxelSize, vol):
    """
    arguments:
        voxelSize -- the xyz physical dimensions of a voxel (in nm)
        vol       -- the volume to calculate the TPB on
   """

    # print "voxelSize x:%g    y: %g    z:%g" % voxelSize
    # print "vol shape: %s" % str(vol.shape)

    (classVol, ccInfo) = classifyVolume(vol)

    # print "Done with classifyVolume"

    # should we do some stats on the ccInfo?

    (lengths, branches) = measureAllTpbPaths(voxelSize, vol, classVol)

    return lengths, branches


# end of calc_tpb (voxelSize, vol)

# ----------------------------------------------------------------------


def writePtGraphEdges(fh, ptGraph, voxelSize):
    # Note that the node locations are stored in voxel index
    # coordinates, so we need to multiply these by voxelSize
    # to put them in the right coordinate system.

    # print "C EdgeActiveFP = ", EdgeActiveFP
    # print "C fh = ", fh

    fh.write("\n# Begin ptGraph")

    # note that the line segments don't come out in any particular order

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections and write out a line segment
        # for each edge
        connections = epn.connections
        for conn in connections:
            if not conn.visited:
                fh.write("\nline\n")
                for i in [0, 1]:
                    coord = (
                        float(conn.ptNode[i].loc[0]) * voxelSize[0],
                        float(conn.ptNode[i].loc[1]) * voxelSize[1],
                        float(conn.ptNode[i].loc[2]) * voxelSize[2]
                    )
                    fh.write("%f %f %f\n" % coord)

                conn.visited = True

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    fh.write("# End ptGraph\n")
    fh.flush()

    return 0


# end of def writePtGraphEdges (fh, ptGraph)


def writeCentroidSeg(fh, connections):
    # this is only called when connects has two elements
    conns = list(connections)
    a = conns[0]
    b = conns[1]

    if a.ptNode[0] == b.ptNode[0]:
        ia = 0
        ib = 0

    elif a.ptNode[0] == b.ptNode[1]:
        ia = 0
        ib = 1

    elif a.ptNode[1] == b.ptNode[0]:
        ia = 1
        ib = 0

    elif a.ptNode[1] == b.ptNode[1]:
        ia = 1
        ib = 1

    else:
        print >> sys.stderr, \
            "Fatal error while traversing TPB connections (A).\n"
        sys.exit(-1)

    fh.write("\nline\n")
    fh.write("%f %f %f\n" % tuple(a.centroid[ia, :]))
    fh.write("%f %f %f\n" % tuple(b.centroid[ib, :]))

    return 0


# end of def writeCentroidSeg (fh, connections)


# ----------------------------------------------------------------------
def writePtGraphSmoothedPath(fh, ptGraph):
    fh.write("\n# Begin ptGraph")

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    # See comments for sumSegmentLengths; this function uses the same
    # traversal scheme

    # note that the line segments don't come out in any particular order

    # totalLength = 0.0

    # first find all segments of type (a) 
    for epn in ptGraph.allPtNodes.values():
        connections = epn.connections
        if len(connections) == 2:
            # write out the segment between the corresponding centroids
            writeCentroidSeg(fh, connections)

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    # now look at each connection and sum the distances between the two 
    # centroids
    for epn in ptGraph.allPtNodes.values():
        # traverse the list of connections and write out the segment
        # between centroids
        connections = epn.connections
        for conn in connections:
            if not conn.visited:
                if not conn.centroidsAreEqual:
                    fh.write("\nline\n")
                    fh.write("%f %f %f\n" % tuple(conn.centroid[0, :]))
                    fh.write("%f %f %f\n" % tuple(conn.centroid[1, :]))
                conn.visited = True

    # clear the visited flags
    setConnectionVisitFlags(ptGraph, False)

    fh.write("# End ptGraph\n")
    fh.flush()

    return 0

# end of def writePtGraphSmoothedPath (fh, ptGraph)

# ----------------------------------------------------------------------

EdgeActiveFP = None
SmoothActiveFP = None
EdgeInactiveFP = None
SmoothInactiveFP = None
EdgeUnknownFP = None
SmoothUnknownFP = None


# ----------------------------------------------------------------------
def writePtGraph(ptGraph, voxelSize):
    global EdgeActiveFP
    global SmoothActiveFP
    global EdgeInactiveFP
    global SmoothInactiveFP
    global EdgeUnknownFP
    global SmoothUnknownFP

    # print "B EdgeActiveFP = ", EdgeActiveFP

    if ptGraph.pathCategory == ACTIVE:
        writePtGraphEdges(EdgeActiveFP, ptGraph, voxelSize)
        writePtGraphSmoothedPath(SmoothActiveFP, ptGraph)

    elif ptGraph.pathCategory == INACTIVE:
        writePtGraphEdges(EdgeInactiveFP, ptGraph, voxelSize)
        writePtGraphSmoothedPath(SmoothInactiveFP, ptGraph)

    elif ptGraph.pathCategory == UNKNOWN:
        writePtGraphEdges(EdgeUnknownFP, ptGraph, voxelSize)
        writePtGraphSmoothedPath(SmoothUnknownFP, ptGraph)

    return 0


# end of def writePtGraph (ptGraph, voxelSize)


# ----------------------------------------------------------------------

def writeBoundingBox(fn, voxelSize, shp):
    bbFP = open(fn, "w")
    bbFP.write("# volume bounding box\n")
    bbFP.write("# voxel size is: " + str(voxelSize) + "\n")
    bbFP.write("# volume voxel dimensions: " + str(shp) + "\n")
    bbFP.write("\n")

    # lower and upper corners of box
    lc = (0.0, 0.0, 0.0)
    uc = (voxelSize[0] * shp[0], voxelSize[1] * shp[1], voxelSize[2] * shp[2])

    # bottom square
    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], uc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], uc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], lc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], lc[2]))
    bbFP.write("\n")

    # top square
    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], uc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], uc[1], uc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], uc[1], uc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], lc[1], uc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], uc[2]))
    bbFP.write("\n")

    # the four vertical edges
    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], lc[1], uc[2]))

    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (lc[0], uc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (lc[0], uc[1], uc[2]))

    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (uc[0], uc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], uc[1], uc[2]))

    bbFP.write("line 0.5 0.5 0.5 1\n")
    bbFP.write("%f %f %f\n" % (uc[0], lc[1], lc[2]))
    bbFP.write("%f %f %f\n" % (uc[0], lc[1], uc[2]))

    bbFP.close()


# end of def writeBoundingBox (fn, voxelSize, shp)


# ----------------------------------------------------------------------

def setupPtGraphGeomOutputs(voxelSize, shp):
    global EdgeActiveFP
    global SmoothActiveFP
    global EdgeInactiveFP
    global SmoothInactiveFP
    global EdgeUnknownFP
    global SmoothUnknownFP

    writeBoundingBox("volumeBoundingBox.savg", voxelSize, shp)

    # open each output .savg file and set the initial color

    EdgeActiveFP = open("edgeActive.savg", "w")
    EdgeActiveFP.write("# Active TPB edges\n")
    EdgeActiveFP.write("line 0.7 0.7 0.7 1\n")

    SmoothActiveFP = open("smoothActive.savg", "w")
    SmoothActiveFP.write("# Active TPB edges (smoothed)\n")
    SmoothActiveFP.write("line 1 1 1 1 \n")

    EdgeInactiveFP = open("edgeInactive.savg", "w")
    EdgeInactiveFP.write("# Inactive TPB edges\n")
    EdgeInactiveFP.write("line 0.0 0.7 0.7 1\n")

    SmoothInactiveFP = open("smoothInactive.savg", "w")
    SmoothInactiveFP.write("# Inactive TPB edges (smoothed)\n")
    SmoothInactiveFP.write("line 0 1 1 1 \n")

    EdgeUnknownFP = open("edgeUnknown.savg", "w")
    EdgeUnknownFP.write("# Unknown TPB edges\n")
    EdgeUnknownFP.write("line 0.7 0.0 0.7 1\n")

    SmoothUnknownFP = open("smoothUnknown.savg", "w")
    SmoothUnknownFP.write("# Unknown TPB edges (smoothed)\n")
    SmoothUnknownFP.write("line 1 0 1 1\n")

    # print "A EdgeActiveFP = ", EdgeActiveFP


# end of def setupPtGraphGeomOutputs ()


# ----------------------------------------------------------------------
def closePtGraphGeomOutputs():
    global EdgeActiveFP
    global SmoothActiveFP
    global EdgeInactiveFP
    global SmoothInactiveFP
    global EdgeUnknownFP
    global SmoothUnknownFP

    EdgeActiveFP.close()
    SmoothActiveFP.close()
    EdgeInactiveFP.close()
    SmoothInactiveFP.close()
    EdgeUnknownFP.close()
    SmoothUnknownFP.close()


# end of def closePtGraphGeomOutputs ()

# ----------------------------------------------------------------------

def usage():
    print >> sys.stderr, \
        "Usage: TBD\n"
    # JGH NEED MORE HERE


# end of def usage ():

# ----------------------------------------------------------------------
def getVoxSizeFromFile(voxSizeFN):
    try:
        # read all lines into a list
        f = open(voxSizeFN)
        lines = f.readlines()
        f.close()

    except EnvironmentError as err:
        print "Error: Unable to read voxel size file ", voxSizeFN
        print "     ", str(err)
        return None

    tokList = list()

    # go through each line and split into list of tokens; this list
    # should be a list of image file names.
    for line in lines:
        tokList = tokList + line.split()

    if len(tokList) < 3:
        print "Error: Voxel size file ", voxSizeFN, " is incomplete."
        return None

    # voxSize = (1.0, 1.0, 1.0)

    try:
        voxSize = (float(tokList[0]), float(tokList[1]), float(tokList[2]))

    except ValueError as err:
        print >> sys.stderr, \
            "Error: Unable to convert voxel size tokens ", tokList[0:3]
        print >> sys.stderr, "      ", str(err)
        return None

    return voxSize


# end of def getVoxSizeFromFile (voxSizeFN) :

# ----------------------------------------------------------------------
#
# parses the command line, which should look like:
#
#    progName  -v x y z  imgFN_0 imgFN_1 ...
#  or
#    progName  --voxelSize x y z  imgFN_0 imgFN_1 ...
#  or
#    progName  voxelSzFN imgFN_0 imgFN_1 ...
#
def getCmdLineOptionsAlt():
    # voxSize = None
    # voxSizeFN = None

    argParser = argparse.ArgumentParser(
        description='Calculate and classify TPB networks in a series of '
                    'two-dimensional label images. If outputting from Avizo, '
                    'save LabelField as 2D tiffs. Labels should be in the '
                    'order of pore, electrode, electrolyte, such that they '
                    'get the labels 1, 2, 3, respectively.'
    )
    argParser.set_defaults(voxelSize=None, voxelSizeFN=None)
    argParser.add_argument("-s", "--voxelSize", nargs=3, type=float,
                           dest="voxelSize",
                           help='voxel sizes, provided in order as <xSize '
                                'ySize zSize>')
    argParser.add_argument("-S", "--voxelSizeFile", type=str,
                           dest="voxelSizeFN",
                           help='Alternatively, a filename can '
                                'be provided that contains the voxel sizes '
                                'in the same manner as -voxelSize.')
    argParser.add_argument('fileNames', nargs='+',
                           type=str,
                           help='one or more file names to process. '
                                'Alternatively, a file containing a list of '
                                'filenames (one per line) can be provided.')

    args = argParser.parse_args()

    if len(sys.argv) == 1:
        argParser.print_help()
        sys.exit()

    voxSize = args.voxelSize
    voxSizeFN = args.voxelSizeFN

    if voxSize is None:
        if voxSizeFN is None:
            voxSize = (1.0, 1.0, 1.0)
        else:
            voxSize = getVoxSizeFromFile(voxSizeFN)
            if voxSize is None:
                print >> sys.stderr, \
                    "Error: Unable to read voxel size file ", voxSizeFN, "."
                sys.exit(1)

    return voxSize, args.fileNames

# endi of getCmdLineOptionsAlt


# ----------------------------------------------------------------------
def getCmdLineOptions():
    xdim = ydim = zdim = nimgs = 0
    indir = "."

    try:
        opts, args = getopt.getopt(sys.argv[1:], "x:y:z:n:i:h",
                                   ["xdim", "ydim", "zdim", "nimgs", "indir",
                                    "help"])

        for opt, val in opts:
            if opt in ("-x", "--xdim"):
                xdim = float(val)
            elif opt in ("-y", "--ydim"):
                ydim = float(val)
            elif opt in ("-z", "--zdim"):
                zdim = float(val)
            elif opt in ("-n", "--nimgs"):
                nimgs = int(val)
            elif opt in ("-i", "--indir"):
                indir = val
            elif opt in ("-h", "--help"):
                usage()
                sys.exit(1)
            else:
                assert False, "unhandled option"

    # thrown inside getopt.getopt
    except getopt.GetoptError, err:
        print(str(err))
        sys.exit(1)
    # thrown if a non-numeric value provided
    except ValueError, err:
        print(str(err))
        sys.exit(1)

    if xdim == 0 or ydim == 0 or zdim == 0 or nimgs == 0:
        print("xdim, ydim, zdim, and nimgs must be specified")
        sys.exit(1)

    return (xdim, ydim, zdim), nimgs, indir


# end of def getCmdLineOptions () :

# ----------------------------------------------------------------------

def printLengthStats(name, lengths, branches, totals):
    branches = np.array(branches)

    num = len(lengths)
    if totals[0] == 0:
        numPercent = 0.0
    else:
        numPercent = 100.0 * float(num) / float(totals[0])

    print ""
    print "Summary of %s TPB paths:" % name
    print "    Number of %s paths                 : %d   %6.2f%% of all " \
          "paths" % (name, num, numPercent)

    if num > 0:
        totLen = np.sum(lengths)
        if totals[1] == 0:
            lenPercent = 0.0
        else:
            lenPercent = 100.0 * float(totLen) / float(totals[1])
        meanLen = np.mean(lengths)
        medianLen = np.median(lengths)
        minLen = np.amin(lengths)
        maxLen = np.amax(lengths)

        numBranching = branches[branches != 0].size
        branchingPercent = 100.0 * float(numBranching) / float(num)
        print \
            "    Number of %s paths with branching  : %d    %6.2f%% of %s " \
            "paths" % (name, numBranching, branchingPercent, name)

        print \
            "    Sum of lengths of %s paths     : %f   %.2f%% of all paths" % \
            (name, totLen, lenPercent)
        print "    Mean of lengths of %s paths    : %f" % (name, meanLen)

        if num > 1:
            stdDev = np.std(lengths)
            print "    Std dev of lengths of %s paths : %f" % (name, stdDev)

        print "    Minimum of lengths of %s paths : %f" % (name, minLen)
        print "    Maximum of lengths of %s paths : %f" % (name, maxLen)
        print "    Median of lengths of %s paths  : %f" % (name, medianLen)

    print ""

    return 0


# end of def printLengthStats (name, lengths):


# ----------------------------------------------------------------------

def printTpbLengths(lengths, branches):
    activePathLengths = lengths[ACTIVE]
    inactivePathLengths = lengths[INACTIVE]
    unknownPathLengths = lengths[UNKNOWN]

    totalNumPaths = (len(activePathLengths) +
                     len(inactivePathLengths) +
                     len(unknownPathLengths))

    totalLength = (np.sum(activePathLengths) +
                   np.sum(inactivePathLengths) +
                   np.sum(unknownPathLengths))

    allBranches = np.array(branches[ACTIVE] +
                           branches[INACTIVE] + branches[UNKNOWN])
    totalBranching = allBranches[allBranches != 0].size

    '''
    print "Total number of all TPB paths: ", totalNumPaths
    print "Total number of branching paths: %d    %.2f%%" % (totalBranching, 
                    100.0*float(totalBranching)/ float(totalNumPaths))
    print "Sum of lengths of all TPB paths: ", totalLength
    print ""
    print ""
    '''

    totals = (totalNumPaths, totalLength, totalBranching)

    printLengthStats("Active", lengths[ACTIVE], branches[ACTIVE],
                     totals)
    printLengthStats("Inactive", lengths[INACTIVE], branches[INACTIVE],
                     totals)
    printLengthStats("Unknown", lengths[UNKNOWN], branches[UNKNOWN],
                     totals)

    printLengthStats("all",
                     lengths[ACTIVE] +
                     lengths[INACTIVE] +
                     lengths[UNKNOWN],
                     branches[ACTIVE] +
                     branches[INACTIVE] +
                     branches[UNKNOWN],
                     totals)


# end of def printTpbLengths (lengths)

# ----------------------------------------------------------------------

# NOTE: I believe that it can be demonstrated that a path can only
#       terminate at the edge of the volume. A TPB edge end vertex
#       that is interior to the volume must connect to another TPB edge.
#
#       Thus there cannot be single edge paths if all voxel dimensions
#       exceed 1. If one of the voxel dimensions is one, then all of the
#       TPB paths will be length one.  If more than one voxel dimension
#       is 1, then there can be no paths.
#
#       

def testVol1():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 2, 2, 2, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 1, 4, 4],
                [1, 1, 1, 4, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 4]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol1 ():

# ----------------------------------------------------------------------
def testVol2():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [2, 2, 2, 2, 2],
                [4, 4, 4, 4, 4],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol2 ():

# ----------------------------------------------------------------------
def testVol3():
    tv = np.array(
        [
            [
                [1, 1, 1],
                [1, 1, 1],
                [1, 1, 1]
            ],
            [
                [1, 1, 1],
                [2, 2, 2],
                [4, 4, 4]
            ],
            [
                [1, 1, 1],
                [1, 1, 1],
                [1, 1, 1]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol3 ():

# ----------------------------------------------------------------------
def testVol_2D():
    tv = np.array(
        [
            [
                [1, 1, 1, 1],
                [1, 1, 1, 1],
                [1, 1, 1, 1],
            ],
            [
                [2, 3, 3, 3],
                [2, 2, 2, 3],
                [2, 2, 2, 3]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_2D ()


# ----------------------------------------------------------------------
def testVol_3D():
    tv = np.array(
        [
            [
                [1, 1, 1, 1],
                [1, 1, 1, 1],
                [1, 1, 1, 1]
            ],
            [
                [2, 3, 3, 3],
                [2, 2, 2, 3],
                [2, 2, 2, 1]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_3D ():


# ----------------------------------------------------------------------
def testVol_loop():
    tv = np.array(
        [
            [
                [1, 1, 1, 1],
                [1, 1, 1, 1],
                [1, 1, 1, 1],
                [1, 1, 1, 1]
            ],
            [
                [2, 2, 2, 2],
                [2, 3, 3, 2],
                [2, 3, 3, 2],
                [2, 2, 2, 2]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_loop ():

# ----------------------------------------------------------------------
def testVol_corner2D():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 2],
                [1, 1, 1, 1, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 2, 2]
            ],
            [
                [3, 2, 1, 1, 3],
                [2, 2, 1, 1, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 2, 3]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_corner2D ():

# ----------------------------------------------------------------------
def testVol_corner3D():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 3, 2, 1, 1],
                [3, 3, 2, 1, 1],
                [2, 2, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_corner3D ():

def testVol_branch1():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 3, 2, 1, 1],
                [1, 3, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]
        ])

    return remap_vol_vals(tv)
# end of def testVol_branch1 ():


def testVol_branch2():
    tv = np.array(
        [

            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 3, 2, 1, 1],
                [3, 3, 2, 1, 1],
                [2, 2, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]
        ])

    return remap_vol_vals(tv)


# end of def testVol_branch2 ():


def testVol_branch3():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 3, 2, 1, 1],
                [1, 3, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 1, 1, 1, 1],
                [1, 1, 2, 2, 2],
                [1, 1, 3, 3, 3],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]

        ])

    return remap_vol_vals(tv)
# end of def testVol_branch3 ():


def testVol_smallLoop():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 3, 2, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]

        ])

    return remap_vol_vals(tv)
# end of def testVol_smallLoop ():


def testVol_branch4():
    tv = np.array(
        [
            [
                [1, 1, 1, 1, 1],
                [1, 1, 3, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 2, 3, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],

            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 3, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 3, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ],
            [
                [1, 1, 1, 1, 1],
                [1, 1, 3, 1, 1],
                [1, 1, 2, 1, 1],
                [1, 1, 1, 1, 1],
                [1, 1, 1, 1, 1]
            ]

        ])

    return remap_vol_vals(tv)
# end of def testVol_branch4 ():


# ----------------------------------------------------------------------
def main():
    # vol = None

    try:

        print "\nBeginning TPB processing ", datetime.now().ctime(), "\n"
        sys.stdout.flush()

        (voxelSize, imgFNs) = getCmdLineOptionsAlt()

        # Load the images. We have several ways of creating
        # test volumes, but the call to loadVolFromFileList
        # is the real file loader, so for a real run, you 
        # should uncomment that line and comment out all of
        # the testVols.
        #
        vol = loadVolFromFileList(imgFNs, voxelSize)
        #
        # Uncomment one of the following to run a test volume.
        #   The OK! comments indicate that I have checked the
        #   results by hand.
        # vol = testVol_2D ()           # OK!
        # vol = testVol_3D ()           # OK!
        # vol = testVol_corner2D ()     # OK!
        # vol = testVol_corner3D ()     # OK!
        # vol = testVol_branch1 ()      # OK!
        # vol = testVol_branch2 ()      # Very similar to corner3D
        #   visually fine; len matches
        # vol = testVol_branch3 ()      # OK!
        # vol = testVol_branch4 ()      # Visually fine; len matches
        #   this is a little too
        #   complicated to check by hand
        # vol = testVol_loop ()         # OK!
        # vol = testVol_smallLoop ()    # OK!

        if vol is None:
            sys.exit(-1)

        '''
        indexPermute = [0,1,2]
        print "Transposing volume with index permutation: ", indexPermute
        vol = np.transpose(vol, indexPermute)
        voxelSize = (voxelSize[indexPermute[0]],  
                     voxelSize[indexPermute[1]],  
                     voxelSize[indexPermute[2]]  )
        print ""
        printVolSize (vol, voxelSize)
        print ""
        '''

        setupPtGraphGeomOutputs(voxelSize, vol.shape)

        (lengths, branches) = calc_tpb(voxelSize, vol)

        closePtGraphGeomOutputs()

        printTpbLengths(lengths, branches)

        print "\nEnding TPB processing ", datetime.now().ctime()
        sys.stdout.flush()

    except IOError, err:
        print(str(err))
        sys.exit(1)

    except ValueError, err:
        print(str(err))
        sys.exit(1)

# end of def main():
# ----------------------------------------------------------------------


# Here's the actual start of the program
if __name__ == "__main__":
    main()
