#!/usr/bin/env bash
#  processOutput "output file from tpbLen.py"
#
#  searches through the output of a TPB run and filters the data down
#  into a nicely formatted version
#
#  set -xv

# input and output files
INPUT=$1
OUTPUT=$1.formatted.dat

# display usage if no arguments given
[ $# -eq 0 ] && { echo "Usage: $(basename $0) <TPB_script output>"; exit 1; }

# check to make sure skeleton file exists
if [ -f "$1" ]; then

# check to make sure we have a valid output file
if [ $(grep -c "Beginning TPB processing" "$INPUT") -eq 0 ]; then
    echo "Input file does not appear to be a TPB-script output."
    echo "Please check the file and try again."
    exit 1
fi

TEMP1=`mktemp -p .`
TMP_OUTPUT=`mktemp -p .`


# make sure that we are using the right output file
while :
do
  if [ -f "$OUTPUT" ]; then
      echo ""
      echo "Default output file \"$OUTPUT\" already exists. Delete (d) or rename (r)?"
      read x
      case ${x} in
       d)
        rm ${OUTPUT}
	echo "Deleted $OUTPUT"
	break
	;;
       r)
        echo "New output file name?"
	read OUTPUT
	;;
       *)
        echo "Invalid response \"$x\""
	;;
      esac
  else
      break
  fi
done

echo "fileName $1" > ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
echo "Execution_time:" >> ${TMP_OUTPUT}
beg=$(grep "Beginning TPB" "$INPUT" | awk '{print $4 " " $5 " " $6 " " $7 " " $8}')
grep "Beginning TPB" "$INPUT" | awk '{print "begTime " $4 "-" $5 "-" $6 "-" $8 " " $7}' >> ${TMP_OUTPUT}
end=$(grep "Ending TPB" "$INPUT" | awk '{print $4 " " $5 " " $6 " " $7 " " $8}')
grep "Ending TPB" "$INPUT" | awk '{print "endTime " $4 "-" $5 "-" $6 "-" $8 " " $7}' >> ${TMP_OUTPUT}

endsec=$(date +%s -d "$end")
begsec=$(date +%s -d "$beg")
let "DAY = ($endsec-$begsec) / 86400"
let "HOUR = ($endsec-$begsec-$DAY*86400) / 3600"
let "MIN = ($endsec-$begsec-$DAY*86400-$HOUR*3600) / 60"
let "SEC = ($endsec - $begsec - $DAY * 86400 - $HOUR * 3600 - $MIN * 60)"
printf -v DAY2 "%02u" ${DAY}
printf -v HOUR2 "%02u" ${HOUR}
printf -v MIN2 "%02u" ${MIN}
printf -v SEC2 "%02u" ${SEC}

echo "time_elapsed $DAY2:$HOUR2:$MIN2:$SEC2 (dd:hh:mm:ss)" >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
echo "Sample_geometry:" >> ${TMP_OUTPUT}
grep "Voxel spacing" "$INPUT" | awk '{print $1"_"$2"_(nm)" " " $4 " " $6 " " $8}' >> ${TMP_OUTPUT}
grep "Volume is" "$INPUT" | awk '{print $1"_"$8 " " $3 " " $5 " " $7}' >> ${TMP_OUTPUT}
grep "Volume physical" "$INPUT" | awk '{print $1"_dim_(nm)" " " $5 " " $7 " " $9}' >> ${TMP_OUTPUT}
grep "Total number of voxels" "$INPUT" | awk '{print "Total_vox:" " " $5}' >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 4 "Number of voxels in each phase" "$INPUT" > ${TEMP1}
echo "Voxels_per_phase:" >> ${TMP_OUTPUT}
awk 'NR==2{print "phase_label " $5"_"$6 " " $7"_"$8"_"$9}' < ${TEMP1} >> ${TMP_OUTPUT}
echo "----------- -------- ----------" >> ${TMP_OUTPUT}
awk 'NR==3{print $1 " " $3 " " $4}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print $1 " " $3 " " $4}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print $1 " " $3 " " $4}' < ${TEMP1} >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 4 "Summary of connected components" "$INPUT" > ${TEMP1}
awk 'NR==1{print "Connected_components:"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==2{print "type number percent"}' < ${TEMP1} >> ${TMP_OUTPUT}
echo "---- ------ -------" >> ${TMP_OUTPUT}
awk 'NR==2{print $3 " " $5 " " $6}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==3{print $3"_"$4 " " $6 " " $7}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print $3 " " $5 " " $6}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print $1 " " $3}' < ${TEMP1} >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 8 "Summary of Active TPB paths" "$INPUT" > ${TEMP1}
echo "TPB_active-summary:" >> ${TMP_OUTPUT}
echo "field value percent" >> ${TMP_OUTPUT}
echo "----- ----- -------" >> ${TMP_OUTPUT}
awk 'NR==2{print "num_Active" " " $6 " " $7 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==3{print "num_ActiveBranching" " " $8 " " $9 " (of_active_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print "sum_LengthActive" " " $8 " " $9 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print "mean_LengthActive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==6{print "stdDev_LengthActive" " " $9}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==7{print "min_LengthActive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==8{print "max_LengthActive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==9{print "median_LengthActive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 8 "Summary of Inactive TPB paths" "$INPUT" > ${TEMP1}
echo "TPB_inactive-summary:" >> ${TMP_OUTPUT}
echo "field value percent" >> ${TMP_OUTPUT}
echo "----- ----- -------" >> ${TMP_OUTPUT}
awk 'NR==2{print "num_Inactive" " " $6 " " $7 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==3{print "num_InactiveBranching" " " $8 " " $9 " (of_inactive_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print "sum_LengthInactive" " " $8 " " $9 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print "mean_LengthInactive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==6{print "stdDev_LengthInactive" " " $9}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==7{print "min_LengthInactive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==8{print "max_LengthInactive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==9{print "median_LengthInactive" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 8 "Summary of Unknown TPB paths" "$INPUT" > ${TEMP1}
echo "TPB_unknown-summary:" >> ${TMP_OUTPUT}
echo "field value percent" >> ${TMP_OUTPUT}
echo "----- ----- -------" >> ${TMP_OUTPUT}
awk 'NR==2{print "num_Unknown" " " $6 " " $7 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==3{print "num_UnknownBranching" " " $8 " " $9 " (of_unknown_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print "sum_LengthUnknown" " " $8 " " $9 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print "mean_LengthUnknown" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==6{print "stdDev_LengthUnknown" " " $9}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==7{print "min_LengthUnknown" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==8{print "max_LengthUnknown" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==9{print "median_LengthUnknown" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}

echo "" >> ${TMP_OUTPUT}
grep -A 8 "Summary of all TPB paths" "$INPUT" > ${TEMP1}
echo "TPB_all-summary:" >> ${TMP_OUTPUT}
echo "field value percent" >> ${TMP_OUTPUT}
echo "----- ----- -------" >> ${TMP_OUTPUT}
awk 'NR==2{print "num_All" " " $6 " " $7 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==3{print "num_AllBranching" " " $8 " " $9 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==4{print "sum_LengthAll" " " $8 " " $9 " (of_all_paths)"}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==5{print "mean_LengthAll" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==6{print "stdDev_LengthAll" " " $9}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==7{print "min_LengthAll" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==8{print "max_LengthAll" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}
awk 'NR==9{print "median_LengthAll" " " $8}' < ${TEMP1} >> ${TMP_OUTPUT}

# transfer data to output file
cat ${TMP_OUTPUT} > ${OUTPUT}

# clean up our mess
if [ -f ${TEMP1} ]; then rm ${TEMP1}; fi
if [ -f ${TMP_OUTPUT} ]; then rm ${TMP_OUTPUT}; fi

exit 0
fi

# couldn't find input file. clean up and quit.
echo "Could not find input file. Check command line parameters."
exit 1
