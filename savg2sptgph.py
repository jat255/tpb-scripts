#!/usr/bin/env python
#
# This script takes a lineset in the SAVG format (from the Scientific
# Applications and Visualization group at NIST) and converts it to an Avizo
# spatial graph (as specified in the Avizo help file, and reproduced here.


# ###########################
#
#  NOT FOR ANALYSIS, JUST VISUALIZATION
#
# ###########################


# #################################
# Avizo Spatial Graph specification
# #################################
# The data type Spatial Graph is used to store data organized in three
# dimensional networks. Points, Nodes, and Segments are the three basic
# structural components used in this module. The Nodes are the branching
# points and endpoints of the network, and the Segments are the lines
# connecting the nodes. Segments, in turn, consist of Points describing the
# three-dimensional course of a segment. Each Point is identified by spatial
# coordinates. A set of Segments connected by Nodes will be termed a Graph
# and a Spatial Graph data object can store even several Graphs. One or
# more scalar data items can be optionally stored for each network item,
# while labels can be associated only with Nodes and Segments.
# In order to store Spatial Graph sets in an Avizo file, three 1D arrays
# have  to be defined, namely EDGES, used to store the Segments indices,
# VERTEX, used to store the Nodes coordinates, and POINT, for the Points.
# Scalar values can be attached to Segments, Nodes and Points, while labels
# and label groups only to Segments and Nodes. Multiple values should be
# distinguished by denoting them Data2, Data3, and so on. Here is an
# example with 5 Nodes, 4 Segments and 24 Points, with one label (
# LabelGroup0) associated to the Segments and one scalar (Data0) to the Points:

# # Avizo 3D ASCII 2.0
#
# define VERTEX 5
# define EDGE 4
# define POINT 24
#
# Parameters {
#   LabelGroup0 { Id 0, Color 0.8 0.16 0.16 }
#   ContentType "HxSpatialGraph"
# }
#
# VERTEX { float[3] VertexCoordinates } @1
# EDGE { int[2] EdgeConnectivity } @2
# EDGE { int NumEdgePoints } @3
# EDGE { int LabelGroup0 } @4
# POINT { float[3] EdgePointCoordinates } @5
# POINT { float Data0 } @6
#
# # Data section
# @1 # Coordinates Nodes
# 0 0 0
# 1 1 1
# 1 0 0
# 0 0 1
# 0 1 0
#
# @2 # To which Nodes the Segments belong
# 0 1
# 2 1
# 1 3
# 3 4
#
# @3 # Number of Points belonging to Edges
# 9
# 6
# 5
# 4
#
# @4 # Label of the Edges
# 0
# 0
# 0
# 0
#
# @5 # Coordinates Points
# 0 0 0
# 0.2 0.2 0.2
# ...
#
# @6 # Scalar associated Points
# 1
# 2
# ...
#
# Once the Nodes are defined, the Segments follow using Nodes indices as
# shown above. For each Segment the number of included Points and their
# coordinates should be specified. The index of the first item is 0. An
# index value of -1 indicates that a item should be terminated.
#
# ###############################
# End Spatial Graph specification
# ###############################

from __future__ import print_function

__author__ = 'Joshua Taillon'

import sys
import getopt
import numpy as np
from tqdm import tqdm
from datetime import datetime
from random import randint
import argparse
try:
    import cPickle as pickle
except ImportError:
    import pickle
import gzip


def getCmdLineOptionsAlt():
    """
    Command line option parser using argparse
    """
    argParser = argparse.ArgumentParser(
        description='Parse an .savg network file into Avizo .am and/or .mv3d '
                    'format. By default, results will be dumped into a pickle '
                    'for easier reloading (since the reading of the .savg '
                    'file can take quite some time).'
    )
    argParser.add_argument("-i", "--infile", type=str,
                           dest="infile",
                           help='input file in .savg format (usually the '
                                'output of the tpbLen.py script). Note, '
                                'if input file does not end with .savg file '
                                'format, it will be interpreted as output '
                                'saved from a previous run, and will be '
                                'loaded accordingly')
    argParser.add_argument("-o", "--outfile", type=str,
                           dest="outfile",
                           help='output file to write')
    argParser.add_argument("-l", "--label", type=str,
                           dest='label', default='1',
                           help='label to include with Avizo SpatialGraph ('
                                'or, the \"thickness\" value to give to the '
                                '.mv3d file)')
    argParser.add_argument("-f", "--format", type=str,
                           dest="format",
                           choices=['mv3d', 'am'],
                           default='mv3d',
                           help='output format to write (either mv3d or am)')
    argParser.add_argument("--no-dump",
                           dest="dump", action='store_false',
                           default=True,
                           help='do not dump results to compressed pickle '
                                'format for easier future loading')
    argParser.add_argument('--debug',
                           dest='debug', action='store_true',
                           default=False,
                           help='flag to control additional output of '
                                'debugging information')

    args = argParser.parse_args()

    if len(sys.argv) == 1:
        argParser.print_help()
        sys.exit()

    if args.outfile == None:
        args.outfile = "{}.{}".format(args.infile, args.format)
        print('Using default output name:', args.outfile)

    return args.infile, args.outfile, args.label, args.format, \
           args.dump, args.debug


# def get_cmd_line_options():
#     """
#     Get command line options during usage
#
#     Returns:
#     --------
#     infile: str
#         input file (.savg)
#     outfile: str
#         output file (.am)
#     label: str
#         label to be put in Avizo spatialgraph
#     print_err: bool
#         switch whether or not to print errors to console
#     """
#     infile = ""
#     outfile = infile + ".am"
#     label = "1"
#     print_err = False
#
#     try:
#         opts, args = getopt.getopt(sys.argv[1:], "i:o:l:e:h",
#                                  ["infile", "outfile", "label", "printerror",
#                                     "help"])
#
#         for opt, val in opts:
#             if opt in ("-i", "--infile"):
#                 infile = str(val)
#                 outfile = infile + ".am"
#             elif opt in ("-o", "--outfile"):
#                 outfile = str(val)
#             elif opt in ("-l", "--label"):
#                 label = str(val)
#             elif opt in ("-e", "--printerror"):
#                 print_err = True
#             elif opt in ("-h", "--help"):
#                 usage()
#                 sys.exit(1)
#             else:
#                 assert False, "unhandled option"
#
#     # thrown inside getopt.getopt
#     except getopt.GetoptError as err:
#         print(str(err))
#         sys.exit(1)
#     # thrown if a non-numeric value provided
#     except ValueError as err:
#         print(str(err))
#         sys.exit(1)
#
#     if infile == "":
#         print("Input file (-i, --infile) must be specified", file=sys.stderr)
#         print("Call `savg2sptgph.py -h` for more help", file=sys.stderr)
#         sys.exit(1)
#
#     return infile, outfile, label, print_err


def print_avizo(out_f_name,
                label,
                num_verts,
                num_edges,
                num_points,
                vertices,
                network_list):
    """
    Write the spatial graph that has been read from a .savg file to a .am file

    :param out_f_name: str
        name of output file that will be written
    :param label: str
        label for this spatial graph that will go in @4 section of .am file
    :param num_verts: int
        number of vertices
    :param num_edges: int
        number of edges
    :param num_points: int
        number of points
    :param vertices: list of str
        list of vertex points in the structure
    :param network_list: list
        list of graphs read from the .savg
    """
    f = open(out_f_name, 'w')
    print("# Avizo 3D ASCII 2.0", file=f)
    print("", file=f)
    print("define VERTEX %i" % num_verts, file=f)
    print("define EDGE %i" % num_edges, file=f)
    print("define POINT %i" % num_points, file=f)
    print("", file=f)
    print("Parameters {", file=f)
    print("    LabelGroup0 { Id 0, Color 0.3 0.8 0.8 }", file=f)
    print("    ContentType \"HxSpatialGraph\"", file=f)
    print("}", file=f)
    print("", file=f)
    print("VERTEX { float[3] VertexCoordinates } @1", file=f)
    print("EDGE { int[2] EdgeConnectivity } @2", file=f)
    print("EDGE { int NumEdgePoints } @3", file=f)
    print("EDGE { int LabelGroup0 } @4", file=f)
    print("POINT { float[3] EdgePointCoordinates } @5", file=f)
    print("POINT { float Data0 } @6", file=f);
    print("", file=f)
    print("# Data section", file=f)
    print("@1 # Coordinates of Nodes", file=f)
    for i in vertices:
        print(i[0], file=f)
        print(i[1], file=f)
    print("", file=f)
    print("@2 # Connections - pairs of node indices, one pair per edge",
          file=f)
    for i in range(0, num_verts, 2):
        print("%i %i" % (i, i+1), file=f)
    print("", file=f)
    print("@3 # Number of Points for each edge", file=f)
    for i in network_list:
        print(len(i), file=f)
    print("", file=f)
    print("@4 # Label for each edge", file=f)
    for i in range(num_edges):
        print(label, file=f)
    print("", file=f)
    print("@5 # Coordinates of points along each edge", file=f)
    for i in network_list:
        for j in i:
            print(j, file=f)
    print("@6 # Scalar associated points", file=f)
    for i in network_list:
        print('1', file=f)
    f.close()
    print("\nWrote spatial graph (.am format) to", out_f_name)


def print_mv3d(out_f_name,
               network_list,
               thickness=1):
    """
    Write the spatial graph that has been read from a .savg file to an
    .mv3d file

    :param out_f_name: str
        name of output file that will be written
    :param network_list: list
        list of graphs read from the .savg
    :param thickness: str
        label for this spatial graph that will go in the last column of the
        .mv3d file
    """
    to_write = network_list

    num_points = 0
    for g in to_write:
        num_points += len(g)

    with open(out_f_name, 'w') as f:
        print("# MicroVisu3D file", file=f)
        print("# Number of lines   {}".format(len(to_write)), file=f)
        print("# Number of points  {}".format(num_points), file=f)
        print("# Number of inter.  0", file=f)
        print("#", file=f)
        print("# No		x		y		z		d", file=f)
        print("#", file=f)

        for i, g in enumerate(tqdm(network_list, desc='Writing .mv3d file')):
            for l in g:
                print(i, l, thickness, file=f)
            print('', file=f)

    print("\nWrote spatial graph (.mv3d format) to", out_f_name)


def parse_pt_graph(in_file, print_err):
    """
    Process a (.savg) ptGraph that is output by the tpbLen.py script.
    This will give the necessary information to write an Avizo spatial graph

    :param in_file: file
        .savg file that has already been opened that will be processed
    :param print_err: bool
        flag whether or not to print error statements
    :return:
        network_list: list
            list of graphs that have been read from in_file
        vertices: list
            list of vertices of paths read from in_file
        num_edges: int
            number of edges in the network
        num_verts: int
            number of vertices in the network
        num_points: int
            number of points along the edges in the network
    """
    # Define finder function
    def _finditem(the_list, item_to_find):
        return [(ind, the_list[ind].index(item_to_find))
                for ind in range(len(the_list))
                if item_to_find in the_list[ind]]

    # list to hold all the graphs
    network_list = []

    # list to hold points in individual graphs
    this_graph_in_order = []

    content = in_file.read()  # read the whole file into a list
    lines = content.splitlines()  # split the list by lines

    beginnings_list = [i for i, x
                       in enumerate(lines) if x == "# Begin ptGraph"]

    for ii, idx in enumerate(tqdm(beginnings_list, desc='Parsing .savg '
                                                        'ptGraphs')):
        beg = idx + 1
        end = lines[beg:].index("# End ptGraph") + beg
        this_graph = lines[beg:end]

        while 'line' in this_graph:   # remove 'line' and
            this_graph.remove('line')
        while '' in this_graph:       # blank lines from list
            this_graph.remove('')

        this_str_array = np.asarray(this_graph)

        # flag to know when we've properly figured out this graph
        completed = False
        fail_point = None
        attempts = 0

        debug_number = -1

        while not completed:
            try:
                endpoints = []
                this_graph_in_order = []

                # Figure out the branches and any endpoints by looping
                # through the array, finding the points that appear only
                # once, and also those that appear more than twice
                branches = []
                for i in range(len(this_str_array)):
                    if np.sum(this_str_array[i] == this_str_array) == 1:
                        endpoints.append(this_str_array[i])
                    if np.sum(this_str_array[i] == this_str_array) > 2:
                        branches.append(this_str_array[i])

                if beg == debug_number:
                    print('endpoints', endpoints)
                    print('branches', branches)

                # split the list of points into edges
                pairs = [this_graph[i:i+2]
                         for i in range(0, len(this_graph), 2)]

                # if beg == debug_number:
                #     print('pairs', pairs)

                # if no endpoints, we have a loop path,
                # so pick a starting point
                if len(endpoints) == 0:
                    if fail_point is None:
                        # loop through trying different starting points
                        endpoints.append(
                            this_str_array[randint(0, len(this_str_array)-1)])
                    else:
                        endpoints.append(fail_point)

                # add one of the endpoints to the graph
                this_graph_in_order.append(endpoints[0])

                # while pairs still has items in it:
                while len(pairs) > 0:
                    results = _finditem(pairs,
                                        this_graph_in_order[
                                            len(this_graph_in_order)-1])
                    if beg == debug_number:
                        print(results)
                    if len(results) == 1:
                            (i, j) = results[0]
                    else:
                        # We found the last point in current graph more than
                        # once in pairs, meaning it is a branching point;
                        # currently, branching paths are just ignored,
                        # meaning total lengths will be less than real,
                        # so there must be a better way
                        # TODO: Fix branching points

                        # find the edge number of the last point in the graph,
                        # and its position on the edge
                        if beg == debug_number:
                            print('inner results:', results)
                        (i, j) = results[randint(0, len(results)-1)]
                    # add the corresponding point in this edge to the graph
                    this_graph_in_order.append(pairs[i][j-1])
                    pairs.pop(i)  # remove this edge from the list of pairs
                # if the list is completed without error, break the trying loop
                completed = True
            except ValueError:
                # one of the failing points is used as the next try
                fail_point = pairs[randint(0, len(pairs)-1)][0]
                # catch Index errors and retry, printing info
                if print_err:
                    print('failed: ' + str(beg) +
                          "; fail_point: " + fail_point, file=sys.stderr)
                attempts += 1
                if attempts > 10:
                    if print_err:
                        print('skipping rest of ' + str(beg) +
                              '  b/c of too many failed tries...',
                              file=sys.stderr)
                    completed = True

        # add this_graph to the list of graphs
        network_list.append(this_graph_in_order)

    # create list of vertices
    vertices = []
    for i in network_list:
        vertices.append([i[0], i[len(i)-1]])

    # number of TPB edges, vertices, and points
    num_edges = len(network_list)
    num_verts = len([item for sublist in vertices for item in sublist])
    num_points = len([item for sublist in network_list for item in sublist])

    return network_list, vertices, num_edges, num_verts, num_points


def dump_parsed_pickle(filename,
                       network_list,
                       vertices,
                       num_edges,
                       num_verts,
                       num_points):
    """
    Dump the results form the parse_pt_graph method into a pickle for easier
    reloading
    """
    print('Saving results to compressed pickle:', filename)
    with gzip.open(filename, 'wb') as f:
        pickle.dump([network_list, vertices, num_edges, num_verts, num_points],
                    f)


def load_parsed_pickle(filename):
    """
    Load the results from the parse_pt_graph method from a pickle saved with
    ``dump_parsed_pickle``
    """
    print('Loading previous results from compressed pickle:', filename)
    with gzip.open(filename, 'rb') as f:
        network_list, vertices, num_edges, num_verts, num_points = \
            pickle.load(f)

    return network_list, vertices, num_edges, num_verts, num_points


# ----------------------------------------------------------------------
def main():
    (infile, outfile, label, fmt, do_dump, print_err) = getCmdLineOptionsAlt()

    try:

        print("\nParsing input file \"" + infile + "\" at",
              datetime.now().ctime(), "\n")
        sys.stdout.flush()

        # Input was not .savg, so try to load as pickle
        if not infile.endswith('.savg'):
            print('Interpreting input file {} as compresed pickle\n'.format(
                infile))
            network_list, vertices, num_edges, num_verts, num_points = \
                load_parsed_pickle(infile)
            do_dump = False  # no need to dump if we already have pickle

        else:
            # read the .savg file into our data structure
            with open(infile, 'r') as in_file:
                network_list, vertices, num_edges, num_verts, num_points =  \
                    parse_pt_graph(in_file, print_err)

        # dump a pickle of the results
        if do_dump:
            print('Saving results to compressed format...')
            dump_parsed_pickle('{}.pklz'.format(infile),
                               network_list,
                               vertices,
                               num_edges,
                               num_verts,
                               num_points)

        # print the spatial graph from this data:
        if fmt == 'am':
            print_avizo(outfile,
                        label,
                        num_verts,
                        num_edges,
                        num_points,
                        vertices,
                        network_list)
        elif fmt == 'mv3d':
            print_mv3d(outfile,
                       network_list,
                       label)

        print("\nFinished savg2sptgrph conversion at", datetime.now(
        ).ctime(), "\n")
        sys.stdout.flush()

    except IOError as err:
        print(str(err), file=sys.stderr)
        sys.exit(1)

    except ValueError as err:
        print(str(err), file=sys.stderr)
        sys.exit(1)
# end of def main():
# ----------------------------------------------------------------------

# Here's the actual start of the program
if __name__ == "__main__":
    main()
