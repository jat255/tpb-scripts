
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

main (int argc, char **argv)
    {
    char line[1000];
    double totalLen = 0;
    int i;
    int numCoords;
    char *label;

    static float nodeCoords[3000000000][3];

    if (argc > 1)
        {
        label = argv[1];
        }
    else
        {
        label = "1";
        }


    fgets (line, sizeof(line), stdin);
    fgets (line, sizeof(line), stdin);

    numCoords = 0;

    while (fgets (line, sizeof(line), stdin) != NULL)
        {
        if (strstr(line, "line") != NULL)
            {
            double a[2][3];
            double l;
            for (i = 0; i < 2; i++)
                {
                if (fgets (line, sizeof(line), stdin) == NULL)
                    {
                    fprintf (stderr, "bad fgets of coords line\n");
                    exit (-1);
                    }
                if ( sscanf (line, "%f %f %f",
                        nodeCoords[numCoords]+0,
                        nodeCoords[numCoords]+1,
                        nodeCoords[numCoords]+2) != 3)
                    {
                    fprintf (stderr, "bad sscanf of coords line:\n");
                    fprintf (stderr, "    %s\n", line);
                    exit (-1);
                    }

                numCoords++;
                }
            }

        }  // end of reading input

    printf ("# Avizo 3D ASCII 2.0\n");
    printf ("\n");
    printf ("define VERTEX %d\n", numCoords);
    printf ("define EDGE %d\n", numCoords/2);
    printf ("define POINT %d\n", numCoords);
    printf ("\n");
    printf ("Parameters {\n");
    printf ("    LabelGroup0 { Id 0, Color 0.3 0.8 0.8 }\n");
    printf ("    ContentType \"HxSpatialGraph\"\n");
    printf ("}\n");
    printf ("\n");
    printf ("VERTEX { float[3] VertexCoordinates } @1\n");
    printf ("EDGE { int[2] EdgeConnectivity } @2\n");
    printf ("EDGE { int NumEdgePoints } @3\n");
    printf ("EDGE { int LabelGroup0 } @4\n");
    printf ("POINT { float[3] EdgePointCoordinates } @5\n");
    printf ("POINT { float Data0 } @6\n");
    printf ("\n");
    printf ("# Data section\n");
    printf ("@1 # Coordinates of Nodes\n");

    for (i = 0; i < numCoords; i++)
        {
        printf ("%f %f %f\n",
                    nodeCoords[i][0], nodeCoords[i][1], nodeCoords[i][2]);
        }

    printf ("\n");
    printf ("@2 # Connections - pairs of node indices, one pair per edge\n");

    for (i = 0; i < numCoords/2; i++)
        {
        printf ("%d %d\n", 2*i, 2*i + 1);
        }

    printf ("\n");
    printf ("@3 # Number of Points for each edge\n");

    for (i = 0; i < numCoords/2; i++)
        {
        printf ("2\n");
        }



    printf ("\n");
    printf ("@4 # Label for each edge\n");

    for (i = 0; i < numCoords/2; i++)
        {
        printf ("%s\n", label);
        }


    printf ("\n");
    printf ("@5 # Coordinates of points along each edge\n");

    for (i = 0; i < numCoords; i++)
        {
        printf ("%f %f %f\n",
                    nodeCoords[i][0], nodeCoords[i][1], nodeCoords[i][2]);
        }

    printf ("\n");
    printf ("@6 # Scalar associated Points\n");

    for (i = 0; i < numCoords; i++)
        {
        printf ("%s\n", label);
        }

    printf ("\n");


    }  // end of main
