
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

main (int argc, char **argv)
    {
    char line[1000];
    double totalLen = 0;
    int i;
    char *thickness;
    int numCoords;
    char *label;

    static float nodeCoords[3000000000][3];

    if (argc > 1)
        {
        thickness = argv[1];
        }
    else
        {
        thickness = "1";
        }


    fgets (line, sizeof(line), stdin);
    fgets (line, sizeof(line), stdin);

    numCoords = 0;

    while (fgets (line, sizeof(line), stdin) != NULL)
        {
        if (strstr(line, "line") != NULL)
            {
            double a[2][3];
            double l;
            for (i = 0; i < 2; i++)
                {
                if (fgets (line, sizeof(line), stdin) == NULL)
                    {
                    fprintf (stderr, "bad fgets of coords line\n");
                    exit (-1);
                    }
                if ( sscanf (line, "%f %f %f",
                        nodeCoords[numCoords]+0,
                        nodeCoords[numCoords]+1,
                        nodeCoords[numCoords]+2) != 3)
                    {
                    fprintf (stderr, "bad sscanf of coords line:\n");
                    fprintf (stderr, "    %s\n", line);
                    exit (-1);
                    }

                numCoords++;
                }
            }

        }  // end of reading input

    printf ("# MicroVisu3D file\n");
    printf ("# Number of lines   %d\n", numCoords/2);
    printf ("# Number of points  %d\n", numCoords);
    printf ("# Number of inter.  0\n");
    printf ("#\n");
    printf ("# No		x		y		z		d\n");
    printf ("#\n");

    int edge = 0;
    for (i = 0; i < numCoords-1; i=i+2)
        {
        printf ("%d %f %f %f %s\n",
                    edge, nodeCoords[i][0], nodeCoords[i][1], nodeCoords[i][2], thickness);
        printf ("%d %f %f %f %s\n",
                    edge, nodeCoords[i+1][0], nodeCoords[i+1][1], nodeCoords[i+1][2], thickness);
        edge++;
        }

    printf ("\n");

    }  // end of main
