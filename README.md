Publication information
=======================
Published to the UMD DRUM archive in July 2016

A (potentially) more up-to-date version of this code should be 
available at https://bitbucket.org/jat255/tpb-scripts
but this snapshot serves as a reference to the code as published
as part of J. Taillon's 2016 PhD Thesis.



TPB Scripts
===========

This folder (not really a full module) contains the Python script used to calculate
the triple phase boundary length of a composite cathode structure, based upon the
input of a series of labeled 2D images. It also contains a few helper programs/scripts
that can be used to process the output of the tpbLen program.


tpbLen.py
---------
The primary script that is used to calcualte the TPB lengths and their activity.
Further information regarding the usage of the script can be found in the comments
of the file. 
    
savgToAvizoSG.c
-------------
A small C program (must be compiled) that will convert the outputted TPB network
from tpbLen.py from the .savg format to the Avizo SpatialGraph (.am) format, for loading
into Avizo. It is not really meant to be a great conversion tool, but should be suitable
at least for visualization of the network in Avizo/Amira.
Compile using any C compiler (gcc should work), and then call the executable
from the command line piping the input and output like so:
    
`savgToAvizoSG < input.savg > output.am`
        
savgToMV3D.c
-------------
A small C program (must be compiled) that will convert the outputted TPB network
from tpbLen.py from the .savg format to the .mv3d format, which can also be loaded
into Avizo. It is not really meant to be a great conversion tool, but should be suitable
at least for visualization of the network in Avizo/Amira.
.mv3d is also an easier format for text manipulation, and this small program was 
written to enable compatibility with the methods in the FIBbootstrap module.
Compile using any c compiler (gcc should work), and then call the executable
from the command line piping the input and output like so:

`savgToMV3D < input.savg > output.mv3d`
        
savg2sptgph.py
--------------
Another conversion program for getting from .savg output to Avizo format, but this time
written in Python. Can be useful, but the C program referenced above is likely better to use.
    
processOutput.sh
----------------
A bash helper script that operates on the text outputted from the tpbLen.py script. 
Basically, it converts the output into a more digestable format, more easily copied into
a spreadsheet for further analysis.
Call from the command line (with an example tpbLen.py output named "tpb_results.txt"):
    
`sh processOutput.sh tpb_results.txt`